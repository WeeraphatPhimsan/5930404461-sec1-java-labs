/* Program: MySimpleWindow
* 
* This java program is for practice how to use GUI(Graphical User Interface) 
* in Advanced Computer Programming course 198130 
* The program has write for simulate a simple graphical interface.
* 
*
* @author Weeraphat Phimsan 593040446-1 section: 1
* @since 21.02.2017
* @version 1
*
*
*
*/
package phimsan.weeraphat.lab6;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class MySimpleWindow extends JFrame {

	protected JButton cancelButton, okButton; // this variable for keep "Cancle" and "OK" Button
	protected JPanel buttonPanel;// keep value of panel for button

	public MySimpleWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	// method for set basic setting of simple window
	protected void setFrameFeatures() {
		setLocation((Toolkit.getDefaultToolkit().getScreenSize().width / 2),
				(Toolkit.getDefaultToolkit().getScreenSize().height / 2));
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();

	}

	// method for adding component for window
	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		buttonPanel = new JPanel();
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		add(buttonPanel, BorderLayout.CENTER);

	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}
		});

	}
}