/* Program: PatientFormV1
* 
* This java program is for practice how to use GUI(Graphical User Interface) 
* in Advanced Computer Programming course 198130 
* The program has write for simulate a simple graphical interface.
* 
* this program has auxiliary method named "addPanel" that help you to add panel and label and its text field
* @author Weeraphat Phimsan 593040446-1 section: 1
* @since 21.02.2017
* @version 1
*
*
*
* NOTE: In order to use "addPanel" method -> you have to add arguments following the style of this method
* Example: addPanel( panelName[Name of panel you want to add], label[label you want to add],[String]Name of label )
*
*/

package phimsan.weeraphat.lab6;
import java.awt.*;
import javax.swing.*;

//This is constructor for Class PatientFormV1 
@SuppressWarnings("serial")
public class PatientFormV1 extends MySimpleWindow {
	public PatientFormV1(String string) {
		super(string);

	}

	JLabel nameLabel, heightLabel, weightLabel, birthLabel; //These variables keep the label for basic patient'information.
	JTextField nameTF, heightTF, weightTF, birthTF, textField; //These variables keep the text field of the Label in above line.
    //['TF' is stand for word "text field" (informPanel is stands for information panel)
	JPanel  informPanel_3,topPanel; //Panel for add above contents 
	final static int textFieldSize = 15;//Size for all text field 

	
	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}
//addPanel method [use for add a panel and label] : description was in above contents
	protected void addPanel(JPanel panel, JLabel label, String labelName) {
		textField = new JTextField(textFieldSize);
		label = new JLabel(labelName);
		panel.add(label);
		panel.add(textField);
		topPanel.add(panel);
		add(topPanel);
	}
	
	protected void addComponents() {
		super.addComponents();
		//create the object for panel, label and text field
		
		informPanel_3 = new JPanel(new GridLayout(4, 2));
		topPanel = new JPanel(new BorderLayout());
		birthLabel = new JLabel("Birthdate:");
		birthTF = new JTextField(textFieldSize);
		birthTF.setToolTipText("ex. 22.02.2000");

		//this process is add panel and label through two ways: 'directly add' and 'add through addPanel method'
		addPanel(informPanel_3, nameLabel, "Name: ");
		informPanel_3.add(birthLabel);
		informPanel_3.add(birthTF);
		addPanel(informPanel_3, weightLabel, "Weight(kg.): ");
		addPanel(informPanel_3, heightLabel, "Height(meter): ");

		topPanel.add(informPanel_3);
		add(topPanel,BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

}
