/*Program: PatientFormV2
* 
* This java program is for practice how to use GUI(Graphical User Interface) with
* new component that is JTextarea and JScrollPane.
* and study how to use OOP concept in Advanced Computer Programming course 198130 
* The program has write for simulate a simple graphical interface.
* 
* this program has been added two component : menu bar and combo box for select type of patient
* (These component has been added under the old components)
* 
* @author Weeraphat Phimsan 593040446-1 section: 1 
* @since 21.02.2017
* @version 3
* (Version 3 of PatientForm java program)
*
*
*/

package phimsan.weeraphat.lab6;

import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class PatientFormV3 extends PatientFormV2 {
	JPanel comboBoxPanel;
	JComboBox patientComboBox;
	JMenu fileMenu, configMenu;
	JMenuItem _New,open,save,exit,color,style;
	JMenuBar menuBar;
	JLabel typeOfPatient = new JLabel("Type: ");
String[] patientType = new String[] {"Inpatient","Outpatient"};

	public PatientFormV3(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		
		//create objects for menu item combo box panel and menu bar
		patientComboBox = new JComboBox (patientType);
		comboBoxPanel = new JPanel(new BorderLayout());
	    fileMenu = new JMenu("File");
	    configMenu = new JMenu("Config");
	    menuBar = new JMenuBar();
	    _New = new JMenuItem ("New");
	    open = new JMenuItem ("Open");  
	    save = new JMenuItem ("Save");
	    exit = new JMenuItem ("Exit");
	    color = new JMenuItem ("Color");
	    style = new JMenuItem ("Style");
	   
	    
	    //add sub menu to file menu
	    fileMenu.add(_New);
	    fileMenu.add(open);
	    fileMenu.add(save);
	    fileMenu.add(exit);
	    
	    //add sub menu to file menu 
	    configMenu.add(color);
	    configMenu.add(style);
	    
	    //add menu to add menu bar
	    menuBar.add(fileMenu);
	    menuBar.add(configMenu);
	  
	    // add type of patient label and combo box to combo box panel
		comboBoxPanel.add(typeOfPatient,BorderLayout.WEST);
		comboBoxPanel.add(patientComboBox,BorderLayout.EAST);
		addedPanel.add(comboBoxPanel,BorderLayout.SOUTH);
		setJMenuBar(menuBar);
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}
