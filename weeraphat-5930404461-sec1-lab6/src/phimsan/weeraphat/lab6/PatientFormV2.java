/* Program: PatientFormV2
* 
* This java program is for practice how to use GUI(Graphical User Interface) with
* new component that is JTextarea and JScrollPane.
* and study how to use OOP concept in Advanced Computer Programming course 198130 
* The program has write for simulate a simple graphical interface.
* 
* this program has been added two component : radio button for choose the gender 
* and text area 
* (These component has been added under the old components)
* 
* @author Weeraphat Phimsan 593040446-1 section: 1 
* @since 21.02.2017
* @version 2
* (Version 2 of PatientForm java program)
*
*
*/
package phimsan.weeraphat.lab6;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;


public class PatientFormV2 extends PatientFormV1 {
	////////////////////////////
	JPanel addedPanel, genderPanel, radButtonPanel1,radButtonPanel2, addressPanel; // these are variable to contain value of new panel
	JLabel genderLabel = new JLabel("Gender:" );// this is label for
	JLabel addressLabel = new JLabel("Address:" ); //gender panel
	JRadioButton maleButton, femaleButton; // radio button for gender male and female
	JTextArea address = new JTextArea("Department of Computer Engineering, Faculty of Engineering," + "Khon Kaen University"
			+ ", Mittraparp Rd., T.NaiMuang, A.Muang, KhonKaen, Thailand, 40002", 2, 35);// text area initialized

	JScrollPane scrollBar; // scroll bar for text area
	ButtonGroup gender;// button group for gender radio button

	// constructor for class OatientFormV2
	public PatientFormV2(String title) {
		super(title);
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	///////////////////////////////////
	// addComponents method
	protected void addComponents() {
		// create object to use in this method here:
		// these are panel object
		super.addComponents();
		radButtonPanel1 = new JPanel(new BorderLayout());
		radButtonPanel1.setBorder(new EmptyBorder(0,0,0,50));
		radButtonPanel2 = new JPanel(new GridLayout(1,2));

		addedPanel = new JPanel(new BorderLayout());
		genderPanel = new JPanel(new BorderLayout());
		
		addressPanel = new JPanel();
		addressPanel.setLayout( new BorderLayout());

		// these are other objects:

		scrollBar = new JScrollPane(address);
		address.setEditable(false);
		address.setFont(new Font("Angsananew", Font.TRUETYPE_FONT, 13));// set the text	style of text area
		address.setLineWrap(true);
		address.setWrapStyleWord(true);

		gender = new ButtonGroup();// create button group for radio button gender
		maleButton = new JRadioButton("male");
		femaleButton = new JRadioButton("female");
		gender.add(maleButton);
		gender.add(femaleButton);
		radButtonPanel2.add(maleButton);
		radButtonPanel2.add(femaleButton);
		radButtonPanel1.add(radButtonPanel2,BorderLayout.CENTER);
		genderPanel.add(genderLabel,BorderLayout.WEST);
		genderPanel.add(radButtonPanel1,BorderLayout.EAST);
		
		////////////// this is only an extra line to separate the

		addressPanel.add(addressLabel,BorderLayout.NORTH);
		addressPanel.add(scrollBar,BorderLayout.PAGE_END);
		////////////// code////////////
		////////////// in order to make no confusing///////////
	
		addedPanel.add(genderPanel, BorderLayout.NORTH);
		addedPanel.add(addressPanel, BorderLayout.CENTER);
		add(addedPanel, BorderLayout.CENTER);

	}

	/////////////////////////////////////////////
	////////////////////////////////////////////
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}
