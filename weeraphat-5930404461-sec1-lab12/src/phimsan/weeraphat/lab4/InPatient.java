package phimsan.weeraphat.lab4;

import java.time.LocalDate;

/*
 * Program : InPatients 
 *
 * This program is to use with KhonKaenPatientsV3.
 * This program has been improved from OutPatient, this program can get 
 * admit date and discharge date from the user.
 *
 *
 * @ param Patient's name, birthday, gender, weight, height, visit date, admit date, and discharge date
 *
 * @ author Weeraphat Phimsan 593040446-1
 *
 * @ Version 3.0
 *
 *
 * 31-01-2017
 *
 * */
public class InPatient extends Patient {

	public KhonKaenPatientsV3 person;
	private String admitDate;
	private String dischargeDate;

	
	// constructor
	public InPatient(String name, String birthdate, Gender gender, double height, double weight, String visitDate) {
		super(name, birthdate, gender, height, weight);

	}

	public InPatient(String name, String birthdate, Gender gender, double weight, double height) {
		super(name, birthdate, gender, weight, height);

	}

	public InPatient(String name, String birthdate, Gender gender, double weight, double height, String admitDate,
			String dischargeDate) {
		super(name, birthdate, gender, weight, height);
		this.admitDate = admitDate;
		this.dischargeDate = dischargeDate;
	}

	
	
	//getter and setter
	public String getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}

	public String getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	
	

	@Override
	public String toString() {
		return "InPatient [" + getName() + ", " + getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg., "
				+ (int) getHeight() + " cm., " + "admitDate = " + LocalDate.parse(admitDate, formats)
				+ ", dischargeDate = " + LocalDate.parse(dischargeDate, formats) + "]";
	}

}
