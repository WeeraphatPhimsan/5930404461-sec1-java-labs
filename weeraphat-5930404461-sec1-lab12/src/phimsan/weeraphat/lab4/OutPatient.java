package phimsan.weeraphat.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/*
 * Program : OutPatients 
 *
 * This program is to use with KhonKaenPatientsV2 
 * 
 * This program has improved from  Patient.java, this program can calculate the number of days between  
 * visit date of two patients.
 *
 *
 *
 * @ param Patient's name, birthday, gender, weight, height, visit date, admit date, and discharge date
 *
 * @ author Weeraphat Phimsan 593040446-1
 *
 * @ Version 1.0
 *
 *
 * 31-01-2017
 *
 * */




public class OutPatient extends Patient {

	// variables declaration : hospitalName and visitDate
	public static String hospitalName = ("Srinakarin");
	private String visitDate;
	public static DateTimeFormatter formater = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);

	@Override
	public String toString() {
		return "OutPatient [" + getName() + ", " + "birthdate = " + getBirthdate() + ", " + getGender() + ", "
				+ getWeight() + " kg., " + (int) getHeight() + " cm. " + "visit date = "
				+ LocalDate.parse(visitDate, formats) + "]";
	}

	// getter and setter
	public OutPatient(String name, String birthdate, Gender gender, double height, double weight) {
		super(name, birthdate, gender, height, weight);

	}

	public OutPatient(String name, String birthdate, Gender gender, double height, double weight, String visitDate) {
		super(name, birthdate, gender, height, weight);
		this.visitDate = visitDate;
	}

	public String getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}

	// displayDaysBetween method : use for calculating the number of days from first patient and second one
	public void displayDaysBetween(OutPatient anonymous) {

		LocalDate dayBefore = LocalDate.parse(anonymous.getVisitDate(), formats);
		LocalDate dayAfter = LocalDate.parse(this.visitDate, formats);
		long duration = ChronoUnit.DAYS.between(dayBefore, dayAfter);
		System.out.println(getName() + " visited after " + anonymous.getName() + " for " + duration + " days.");
	}
}
