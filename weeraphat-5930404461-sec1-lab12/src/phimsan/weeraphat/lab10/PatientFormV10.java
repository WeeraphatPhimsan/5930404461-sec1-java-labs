/*
 * Program : PatientForm 
 *This program is improved version of 'PatientV9' that has more features. 
 * The features of this program are :
 * - add patient information to the archive by click 'Add' button
 * - display the added patient by click 'Display' menu item
 * This version also has new sub menu named 'Data'
 * and its menu items : 'Display', 'Search', 'Sort', and 'Remove' 
 *  
 *
 * The propose of this program is to learn about 'Dynamic Array' and 
 * how to use it to keep added patient information to the program. 
 * 
 * 
 * @version 10
 * @date: 04.04.60
 * @author: Weeraphat Phimsan ID: 593040446-1
 */

package phimsan.weeraphat.lab10;

import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import phimsan.weeraphat.lab4.*;
import phimsan.weeraphat.lab9.PatientFormV9;

// this is the class PatientFormV10
public class PatientFormV10 extends PatientFormV9 {

	private static final long serialVersionUID = 1L;

	// this is constructor of this program
	public PatientFormV10(String title) {
		super(title);
	}

	// below codes are object declaring
	JMenu dataMenu;// declare object for 'Date' sub menu
	JMenuItem displayMI, searchMI, sortMI, removeMI;// declare object for menu
													// items of Date sub menu
	protected ArrayList<Patient> patientStorage = new ArrayList<Patient>();// this
																			// array
																			// list
	// to keep
	// patient
	// information
	Gender patientGen;// this variable is keep ENUM gender from lab 4

	// method 'createAndShowGUI' is to set basic properties of window
	public static void createAndShowGUI() {
		PatientFormV10 patientV10 = new PatientFormV10("Patient Form V10");
		patientV10.addComponents();
		patientV10.setFrameFeatures();
		patientV10.addListener();
		patientV10.setAccelatorKey();
		patientV10.setMnemonicKey();
	}

	// menuItnit is method for initialize menu item menu or sub menu
	public void menuInit() {
		dataMenu = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		searchMI = new JMenuItem("Search");
		sortMI = new JMenuItem("Sort");
		removeMI = new JMenuItem("Remove");
	}

	// this method is to add components into the panel
	@Override
	public void addComponents() {
		super.addComponents();
		menuInit();// call method menuIntit
		/// add these menu item to 'Data' sub menu
		dataMenu.add(displayMI);
		dataMenu.add(sortMI);
		dataMenu.add(searchMI);
		dataMenu.add(removeMI);
		menuBar.add(dataMenu);
	}

	// add listener to 'Display' menu item
	public void addListener() {
		super.addListener();
		displayMI.addActionListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object source = event.getSource();
		if (source == okButton) {
			addPatient();
		} else if (source == displayMI) {
			displayPatients();
		}
	}

	// this method is to add patient to the array and also display the
	// information of the patient
	private void addPatient() {
		// show dialog with message of patient's information
		Patient patient = new Patient(nameTxtField.getText(), dateTxtField.getText(),
				(maleRadioB.isSelected() ? Gender.MALE : Gender.FEMALE), Double.parseDouble(weightTxtField.getText()),
				Double.parseDouble(heightTxtField.getText()));

		patientStorage.add(patient);// add object patient to the array list
	}

	// this is method to displays patient's information
	// When use click on display menu item in data sub menu the program will
	// show dialog with
	// message that tell information of patient.
	protected void displayPatients() {
		StringBuilder patientDisplay = new StringBuilder("");
		for (int index = 0; index < patientStorage.size(); index++) {
			patientDisplay.append((index + 1) + ": " + patientStorage.get(index));
			patientDisplay.append(System.getProperty("line.separator"));
		}
		JOptionPane.showMessageDialog(null, patientDisplay);
	}

	// this is the main method to run this program
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}// end of this class
