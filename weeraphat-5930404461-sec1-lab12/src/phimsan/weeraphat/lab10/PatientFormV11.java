/*
 * Program: PatientForm 
 * 
 * This program is an improved version of PatientFomV10
 * This Program has one more features the extended from the old version.
 * In this program you can sort the patient in array list by weight in increasing
 * order. 
 *  
 * 
 * @create: 04.04.60
 * @author Weeraphat Phimsan id 593040446-1
 * @version 11
 * */

package phimsan.weeraphat.lab10;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.SwingUtilities;

import phimsan.weeraphat.lab4.Patient;

public class PatientFormV11 extends PatientFormV10 {
	private static final long serialVersionUID = 100L;

	public PatientFormV11(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV11 patientV11 = new PatientFormV11("Patient Form V11");
		patientV11.addComponents();
		patientV11.setFrameFeatures();
		patientV11.addListener();
		patientV11.setAccelatorKey();
		patientV11.setMnemonicKey();
		
	}

	public void addListener() {
		super.addListener();
		sortMI.addActionListener(this);
	}

	// the method is to check if user click on sort menu item
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object source = event.getSource();
		// check if user click on sort menu item then sort
		// patient by weight and show patient list after sorted
		if (source == sortMI) {
			sortPatients();
			displayPatients();
		}
	}

	// this is method to sort patient object by weight
	protected void sortPatients() {
	//use for loop to compare with any two members of this array until all members are sorted
		for (int index = 0; index < patientStorage.size(); index++) {
			Collections.sort(patientStorage, new Comparator<Patient>() {
				public int compare(Patient patient1, Patient patient2) {
					{
						if (patient1.getWeight() < patient2.getWeight()) {
							Collections.swap(patientStorage, patientStorage.indexOf(patient1), patientStorage.indexOf(patient2));
						}
						return 0;
					}
				}
			});
		}
	}

	// the method is to check the information to run this program
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
