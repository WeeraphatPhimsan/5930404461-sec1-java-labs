/*Program: Patient Form
 * 
 * This Program is to handle exception and defined your own exception.
 * In this program following cases will cause exception as describe below:
 * 
 * 
 * if name , date, weight, height ,address text field and radio button not been selected
 * will show message that these component not have value 
 * if you enter weight in integer form and height in double form
 * will show message "Please enter weight in double and height in int"
 * if you enter date in incorrect format
 * will show the message that warn you to input date in format DD.MM.YYYY
 * 
 * This program contain the following classes : NoNameException, NoBirthDateException, NoAddressException NoGenderException
 * that is custom exception class to handle no name, birth date, gender not been selected , or address is null string case. 
 * 
 * @author Weeraphat Phimsan ID: 593040446-1
 * @create 02/05/60
 * @version 14
 * 
 * */
package phimsan.weeraphat.lab12;

import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

class NoBirthDateException extends Exception {

	private static final long serialVersionUID = 182163336207911390L;

	public NoBirthDateException() {
	}
}

// class NoAddressException for no address entered case

@SuppressWarnings("serial")
class NoAdressExeption extends Exception {
	public NoAdressExeption() {
	}
}

// class NoAddressException for no address entered case
@SuppressWarnings("serial")
class NoNameException extends Exception {
	public NoNameException() {
	}
}

// class NoAddressException for no address entered case
@SuppressWarnings("serial")
class NoGenderException extends Exception {

	public NoGenderException() {

	}
}

///////////////////////// this is the main class/////////////////////////////
public class PatientFormV14 extends PatientFormV13 {
	private static final long serialVersionUID = 1L;

	public PatientFormV14(String title) {
		super(title);
	}

	/* this is action performed method */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			//////////// check in valid information////////////////////
			try {

				// check if name text field is empty or not
				if (nameTxtField.getText().equals("")) {
					throw new NoNameException();
				}
				// check if date text field is empty or not
				if (dateTxtField.getText().equals("")) {

					throw new NoBirthDateException();
				}
				// check if in case that date text field not empty not empty,
				// check if it in correct form
				if (dateTxtField.getText() != null) {
					dateFormatCheck(dateTxtField.getText());
				}
				// check if name text field is empty or not
				if (weightTxtField.getText().equals("")) {
					throw new NumberFormatException();
				}
				// check if weight text field is not in double format
				// check by using . that can distinct integer and double
				if (weightTxtField.getText() != null) {
					if (weightTxtField.getText().contains(".")) {
					} else {
						throw new NumberFormatException();
					}
				}
				// check if weight text field is empty or not
				if (heightTxtField.getText().equals("")) {
					throw new NumberFormatException();
				}
				// check if entered height value is not i integer form that
				// should not contain "."
				if (heightTxtField.getText().contains(".")) {
					throw new NumberFormatException();
				}
				// check if gender is selected or not
				if (genderGrpB.getSelection() == null) {
					throw new NoGenderException();
				}
				// check address is empty or not
				if (addrTxtArea.getText().equals("")) {
					throw new NoAdressExeption();
				} else {
					super.actionPerformed(e);
				}
				// below code is catch exception from above code about the
				// exception as describe at the begin of program
			} catch (NoNameException noNameException) {
				JOptionPane.showMessageDialog(null, "Name has not been entered");
			} catch (NumberFormatException numberFormatException) {
				JOptionPane.showMessageDialog(null, "Please enter weight in double and height in int");
			} catch (NoGenderException noGenderException) {
				JOptionPane.showMessageDialog(null, "No gender have been selected");
			} catch (NoAdressExeption noAdressExeption) {
				JOptionPane.showMessageDialog(null, "No address have been entered");
			} catch (NoBirthDateException birthDateException) {
				JOptionPane.showMessageDialog(null, "No Birthdate have been entered");
			} catch (DateTimeParseException dateTimeParseException) {
				JOptionPane.showMessageDialog(null, "Please enter Birthdate in format DD.MM.YYYY ex. 22.02.2000");
			}

			/////////////////// everything is ok///////////////////////
			// if all informations are valid

		} /* not ok button */
		else {
			super.actionPerformed(e);
		}
	}

	/* this method to test if entered date can create patient */
	public boolean dateFormatCheck(String birthDateIn) throws DateTimeParseException {
		String birthDate = birthDateIn;
		Date date = null;
		try {
			SimpleDateFormat validForm = new SimpleDateFormat("DD.MM.YYYY");// correct format that use to create patient object 
		    //check that entered date is correct format
			date = validForm.parse(birthDate);
			if (!birthDateIn.equals(validForm.format(date))) {
				date = null;
				return false;
			}
		} catch (ParseException e) {
			throw new DateTimeParseException("Please enter Birthdate in format DD.MM.YYYY ex. 22.02.2000", birthDate,
					2);
		}
		return true;
	}// end of method

	/* main method to run this program */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}// end of this method

	/* method createAndShowGUI is to setup basic features of the program */
	public static void createAndShowGUI() {
		PatientFormV14 patientV14 = new PatientFormV14("Patient Form V14");
		patientV14.addComponents();
		patientV14.setFrameFeatures();
		patientV14.addListener();
		patientV14.setMnemonicKey();
		patientV14.setAccelatorKey();
	}// end of this method

}// end of this class