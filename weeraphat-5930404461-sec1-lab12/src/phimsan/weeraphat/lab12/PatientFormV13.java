/*Program: Patient Form
 * 
 * This Program is to show about how to save and write file in java language with 
 * object properties.
 * 	In this version of patient form you can save and open patient, saved patient will be save in 
 * patient's information form that contain name, date of birth, weight, height ,and address.
 * After save progress has done, the array list that keep object patient will be cleared, if you want to
 * called it back you have to use open command in file menu to recall deleted patient. 
 * 
 * 
 * 
 * 
 * 
 * @author Weeraphat Phimsan ID: 593040446-1
 * @create 02/05/60
 * @version 13
 * 
 * */

package phimsan.weeraphat.lab12;

import java.awt.event.ActionEvent;
import java.io.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import phimsan.weeraphat.lab10.PatientFormV12;
import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab4.Patient;

public class PatientFormV13 extends PatientFormV12 {

	private static final long serialVersionUID = 1L;
	protected ObjectInputStream objectInputStream;
	protected ObjectOutputStream objectOutputStream;
	protected FileReader fileReader;
	protected PrintWriter printWriter;
	protected File file;

	protected JFileChooser fileChooser;

	public PatientFormV13(String title) {
		super(title);
	}

	///////////////////////////////////////////////////////////////////////////////
	/////////////////////////// save method is to save patient data
	/////////////////////////////////////////////////////////////////////////////// //////////////////////////////////
	public void save() {
		fileChooser = new JFileChooser();
		int userSelection = fileChooser.showSaveDialog(this);
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			file = fileChooser.getSelectedFile();
			try {
				// the below code is to write patient information in format
				// NAME =
				// BirthDate =
				// Weight =
				// Height =
				// Gender =
				// Address =
				// and follow by its values
				printWriter = new PrintWriter(file);
				for (int i = 0; i < patientStorage.size(); i++) {
					printWriter.println("Name = " + patientStorage.get(i).getName());
					printWriter.println("Birthdate = " + patientStorage.get(i).getBirthdate());
					printWriter.println("Weight = " + patientStorage.get(i).getWeight());
					printWriter.println("Height = " + patientStorage.get(i).getHeight());
					printWriter.println("Gender = " + patientStorage.get(i).getGender());
				}
				JOptionPane.showMessageDialog(null, "You have saved patient datato file " + file.getName());

				printWriter.close();

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} finally {
				System.out.println("Finishe!!");
				System.out.println(patientStorage.size());
				patientStorage.clear();
				System.out.println(patientStorage.size());

			}

		}

	}// end of this method

	////////////////////////////////////////////////////////////////////
	////////////////////// open method is to call patient data/////////
	///////////////////////////////////////////////////////////////////
	public void open() {
		fileChooser = new JFileChooser();
		int patientCheck = 0;// check that patient has created and add to array
								// list
		int selection = fileChooser.showOpenDialog(this);
		String name = "", birthDate = "", gender = "MALE";// name and birthDate
															// string are set to
															// be empty string
															// but gender is set
															// to be "MALE"
															// because it will
															// use to compare
															// with fetched
															// value

		Gender genderPatient;// this is gender of patient that will get in
		double weight = 0;// this variable is represent the fetched height that
							// initialized to zero
		int height = 0;// this variable is represent the fetched weight that
						// initialized to zero

		if (selection == JFileChooser.APPROVE_OPTION) {
			file = fileChooser.getSelectedFile();
			String str1 = "";
			
			try {
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				
				JOptionPane.showMessageDialog(null, "You have opened file " + file.getName());
				// name = bufferedReader.readLine().split("= ")[1];
				while ((str1 = bufferedReader.readLine()) != null) {
					// below condition is to get information from text file that
					// are name, birth date, weight, height, gender, and address
					// for example you know that the line that begin with upper
					// case 'N' will be contained name of patient
					// then I check for the line that has it and get name of
					// patient,
					// and use split.() method to get the value that behind the
					// "=" sign
					// like other informations.
					if (str1.contains("N")) {
						name = str1.split("= ")[1];
					}
					if (str1.contains("B")) {
						birthDate = str1.split("= ")[1];
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
						java.util.Date parsedDate = simpleDateFormat.parse(birthDate);
						birthDate = simpleDateFormat.format(parsedDate);
					}
					if (str1.contains("W")) {
						weight = Double.parseDouble(str1.split("= ")[1]);
					}
					if (str1.contains("H")) {
						height = Integer.parseInt(str1.split("= ")[1]);
					}
					if (str1.contains("G")) {
						patientCheck++;
						if (gender.equalsIgnoreCase(str1.split("= ")[1])) {
							genderPatient = Gender.MALE;
						} else {
							genderPatient = Gender.FEMALE;
						}

						if (patientCheck >= 1) {
							Patient fetchPatient = new Patient(name, birthDate.replace("-", "."), genderPatient, weight,
									height);
							patientStorage.add(fetchPatient);
						}
					}
				}

				bufferedReader.close();

			} catch (

			Exception e1) {
				e1.printStackTrace();
			}

		}
	}// end of this method

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == openMI) {
			open();
		} else if (e.getSource() == saveMI) {
			save();
		} else {
			super.actionPerformed(e);
		}

	}

	/*
	 * method createAndShowGUI is to set up basic features of window like
	 * components and short cut key
	 */
	public static void createAndShowGUI() {
		PatientFormV13 patientV13 = new PatientFormV13("Patient Form V13");
		patientV13.addComponents();
		patientV13.setFrameFeatures();
		patientV13.addListener();
		patientV13.setMnemonicKey();
		patientV13.setAccelatorKey();
	}//end of this method

	/*method main to run this program*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}