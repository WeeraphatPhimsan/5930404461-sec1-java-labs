package phimsan.weeraphat.lab9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PatientFormV9 extends PatientFormV8 {

	private static final long serialVersionUID = 1L;
	JFileChooser fileChooser = new JFileChooser();
	SetColorChooser colorChooser = new SetColorChooser();
	JPanel colorChooserPanel = new JPanel(new BorderLayout());

	public PatientFormV9(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV9 patientV9 = new PatientFormV9("Patient Form V9");
		patientV9.addComponents();
		patientV9.addListener();
		patientV9.setFrameFeatures();
		patientV9.setShortCutKeys();
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (event.getSource() == saveMI) {

			int returnVal = fileChooser.showSaveDialog(PatientFormV9.this);
			File selectedFile = fileChooser.getSelectedFile();
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(null, "You have been saved " + selectedFile.getName());
			} else {
				JOptionPane.showMessageDialog(null, "The save command canceled by user");
			}
		}
		if (event.getSource() == openMI) {
			int retValue = fileChooser.showOpenDialog(PatientFormV9.this);
			File selectedFile = fileChooser.getSelectedFile();
			if (retValue == fileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(null, "You have been opened " + selectedFile.getName());
			} else {
				JOptionPane.showMessageDialog(null, "The Open command canceled by user");
			}
		}
		if (event.getSource() == customMI) {
			Color color = Color.BLACK;
			JOptionPane.showMessageDialog(null, colorChooser);
			if (color == Color.BLACK) {
				changeFontColor(colorChooser.getColor());
			}
		}
	}

	public void addListener() {
		super.addListener();
		exitMI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		customMI.addActionListener(this);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}
}
