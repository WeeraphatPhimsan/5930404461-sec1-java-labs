package phimsan.weeraphat.lab9;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import phimsan.weeraphat.lab8.PatientFormV6;

public class PatientFormV7 extends PatientFormV6 implements ChangeListener {

	// <<<<<<<<<<<<<<<<decalring object>>>>>>>>>>>>>>>
	JPanel bloodPresPanel, new_AddedInfoPanel;

	JSlider bloodSliderTop, bloodSliderButtom;
	JLabel topNumLabel, buttomNumLabel;
	TitledBorder bloodPresTitle;

	public PatientFormV7(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.addListener();
		patientForm7.setFrameFeatures();

	}

	@Override
	public void actionPerformed(ActionEvent event) {

		// his condition is to check if user click "OK" button
		// - show the dialog with information that typed by user
		if (event.getSource() == cancelButton) {
			super.actionPerformed(event);
		}
		if (event.getSource() == okButton) {
			JOptionPane.showMessageDialog(null,
					"Name = " + nameTxtField.getText() + " Birthdate = " + dateTxtField.getText() + " Weight = "
							+ weightTxtField.getText() + "Height = " + heightTxtField.getText() + "\n" + "Gender = "
							+ (maleRadioB.isSelected() ? "Male" : "Female") + "\nAddress = " + addrTxtArea.getText()
							+ "\n" + "Type = " + typeList.getSelectedItem() + "\nBlood Pressure :"
							+ bloodSliderTop.getName() + " = " + bloodSliderTop.getValue() + ", "
							+ bloodSliderButtom.getName() + " = " + bloodSliderButtom.getValue());

		}

	}

	public void addListener() {
		super.addListener();
		bloodSliderTop.setName("Top number");
		bloodSliderTop.addChangeListener(this);
		bloodSliderButtom.setName("Buttom number");
		bloodSliderButtom.addChangeListener(this);
	}

	@Override
	public void addComponents() {
		super.addComponents();
		bloodPresTitle = BorderFactory.createTitledBorder("Blood Pressure");
		bloodPresPanel = new JPanel(new GridLayout(2, 2));
		topNumLabel = new JLabel("Top number:");
		buttomNumLabel = new JLabel("Buttom number:");
		bloodSliderTop = new JSlider(0, 200);
		bloodSliderTop.setMajorTickSpacing(50);
		bloodSliderTop.setMinorTickSpacing(10);
		bloodSliderTop.setPaintTicks(true);
		bloodSliderTop.setPaintLabels(true);

		bloodSliderButtom = new JSlider(0, 200);
		bloodSliderButtom.setMajorTickSpacing(50);
		bloodSliderButtom.setMinorTickSpacing(10);
		bloodSliderButtom.setPaintTicks(true);
		bloodSliderButtom.setPaintLabels(true);

		new_AddedInfoPanel = new JPanel(new BorderLayout());

		bloodPresPanel.setBorder(bloodPresTitle);
		bloodPresPanel.add(topNumLabel);
		bloodPresPanel.add(bloodSliderTop);
		bloodPresPanel.add(buttomNumLabel);
		bloodPresPanel.add(bloodSliderButtom);

		new_AddedInfoPanel.add(genderPanel, BorderLayout.NORTH);
		new_AddedInfoPanel.add(bloodPresPanel, BorderLayout.CENTER);

		contentPanel.add(new_AddedInfoPanel, BorderLayout.CENTER);

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			int value = source.getValue();
			String name = source.getName();
			JOptionPane.showMessageDialog(null, name + " of Blood Pressure is " + value);
		}

	}

}
