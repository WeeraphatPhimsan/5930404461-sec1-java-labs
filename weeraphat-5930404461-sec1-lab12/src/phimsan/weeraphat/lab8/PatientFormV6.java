/*
 * Program Patient Form V6
 * This program is version 6 of PatientForm.java
 * and extends from PatienFormV5
 * 
 * This Program has add icon of sub menu : save, open, and exit
 * This Program also add image of patient 'Manee'
 * 
 * Extra method is setMenuItemIcon is to set icon of menu item
 * by pass the name of menu item want to be set icon and path of image to be the icon
 * 
 * @author: Weeraphat Phimsan 593040446-1
 * @version 6
 * @date: 15/03/60
 * */
package phimsan.weeraphat.lab8;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PatientFormV6 extends PatientFormV5 {

	JPanel lagestPanel = new JPanel();
	private static final long serialVersionUID = -1686571504876508268L;

	public PatientFormV6(String title) {
		super(title);
	}

	@Override
	public void addComponents() {
		super.addComponents();
		JLabel maneePicAdded = new JLabel(new ImageIcon("images\\manee.jpg"));
		setMenuItemIcon(openMI, "images\\openIcon.png");
		setMenuItemIcon(saveMI, "images\\saveIcon.png");
		setMenuItemIcon(exitMI, "images\\quitIcon.png");
		lagestPanel.setLayout(new BorderLayout(0, 30));
		lagestPanel.add(maneePicAdded, BorderLayout.NORTH);
		lagestPanel.add(overallPanel, BorderLayout.SOUTH);
		setContentPane(lagestPanel);
	}

	public void setMenuItemIcon(JMenuItem menuItem, String imagePath) {
		menuItem.setIcon(new ImageIcon(imagePath));
	}

	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.addListener();
		patientForm6.setFrameFeatures();

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}
}
