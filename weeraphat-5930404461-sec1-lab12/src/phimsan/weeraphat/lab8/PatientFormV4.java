/*
 * Program Patient Form V4
 * This program is version 4 of PatientForm.java
 * The program has more features added from PatientFormV3
 * that is when use click on 'OK' button the dialog
 * of input information will appear and  when use click on 'cancel' button 
 * all text will be erased.
 * 
 * @author: Weeraphat Phimsan 593040446-1
 * @version 4
 * @date: 15/03/60
 * */
package phimsan.weeraphat.lab8;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import phimsan.weeraphat.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {
	private static final long serialVersionUID = -8336532530097397466L;
	JDialog informDialog = new JDialog(); // inform dialog is to show message of
	JDialog genderUpdate, typeUpdate; // patient'sinformation
	String massage;// keep massage that
	JButton okButton2 = new JButton("OK"); // this is button for dialog

	public PatientFormV4(String string) {
		super(string);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		super.addMenus();

	}

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.addListener();
		patientForm4.setFrameFeatures();
	}

	protected void addListener() {
		super.okButton.addActionListener(this);// add action listener to 'OK'
												// button
		super.cancelButton.addActionListener(this);// add action listener to
													// 'Cancel' button
		super.maleRadioB.addItemListener(this);// add item listener to 'male'
												// radio button
		super.femaleRadioB.addItemListener(this);// add item listener to
													// 'female' radio button
		super.typeList.addItemListener(this);// add item listener to 'list. in
												// combo box
	}

	// actionPerformed method
	@Override
	public void actionPerformed(ActionEvent event) {
		// this condition is to check if user click "OK" button
		// - show the dialog with information that typed by user
		if (event.getSource() == okButton) {
			JOptionPane.showMessageDialog(null,
					"Name = " + nameTxtField.getText() + " Birthdate = " + dateTxtField.getText() + " Weight = "
							+ weightTxtField.getText() + "Height = " + heightTxtField.getText() + "\n" + "Gender = "
							+ (maleRadioB.isSelected() ? "Male" : "Female") + "\nAddress = " + addrTxtArea.getText()
							+ "\n" + "Type = " + typeList.getSelectedItem());

		}

		// this condition check if user click "Cancel" button
		// - clear all text in all text fields and the text area
		if (event.getSource() == cancelButton) {

			nameTxtField.setText(" ");
			dateTxtField.setText(" ");
			weightTxtField.setText(" ");
			heightTxtField.setText(" ");
			addrTxtArea.setText("");
		}

	}

	@Override
	// method item state changed is to check the state of radio button or combo
	// box has changed
	public void itemStateChanged(ItemEvent itemEvent) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;

		// object selectedRadButton for get source object that is radio button
		Object selectedRadButton = itemEvent.getSource();
		// this condition is check if user changed gender
		// - show dialog with message "Your gender is now changed to male or
		// female"
		if (selectedRadButton == maleRadioB) {

			// "Your gender type is now changed to " + (selectedRadButton ==
			// maleRadioB ? "male" : "female"));
			// below command is to set default gender dialog

			JOptionPane pane = new JOptionPane(
					"    Your gender is now changed to " + (selectedRadButton == maleRadioB ? "male" : "female"));
			JDialog dialog = pane.createDialog((JFrame) null, "Gender Info");
			dialog.setLocation(x, y + this.getHeight()+10);
			dialog.setVisible(true);

		}

		// this condition check if user change the patient's type
		// - show the dialog with message "YOur patient type is now changed to
		// [male or female] "
		if (itemEvent.getSource() == typeList) {
			if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
				JOptionPane pane = new JOptionPane("Your patient type is now change to " + typeList.getSelectedItem());
				JDialog dialog = pane.createDialog((JFrame) null, "Message");
				dialog.setVisible(true);
			}
		}

	}

}
