package phimsan.weeraphat.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import phimsan.weeraphat.lab4.*;
import phimsan.weeraphat.lab4.Patient;

/*
 * Program : Patient.java
 *
 *      This program is to get patient's information from user
 * ( the information contains Patient's name, birthday, gender, 
 * weight, height, visit date, admit date, and discharge date )
 *
 *
 *
 * @ param Patient's name, birthday, gender, weight, height, visit date, admit date, and discharge date
 *
 * @ author Weeraphat Phimsan 593040446-1
 *
 * @ Version 1.0
 *
 *
 * 31-01-2017
 *
 * */

public class PatientV3 extends SickPeople {

	// variables : name birthday gender weight and height of the patient
	private String name;
	private Gender gender;
	private LocalDate birthDate;
	public static DateTimeFormatter formats = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);
	private double Height, Weight;

	public interface HasInsurance {
		void pay();

		double pay(double amount);
	}

	interface HasPet {
		void feedPet();

		void playWithPet();

	}

	interface UnderLegalAge {
		void askPermission();

		String askPermission(String legalGuardianName);
	}

	// constructor
	public PatientV3(String name, String birthdate, Gender gender, double weight, double height) {
		super();
		this.name = name;
		this.birthDate = LocalDate.parse(birthdate, formats);
		this.Height = (int) height;
		this.Weight = weight;
		this.gender = gender;
	}

	// override the method
	@Override
	public String toString() {
		return "Patient [" + name + ", " + birthDate + ", " + gender + ", " + Weight + " kg." + "," + (int) Height
				+ " cm." + "]";
	}

	// getter and setter for each of variables
	public static DateTimeFormatter getFormats() {
		return formats;
	}

	public static void setFormats(DateTimeFormatter formats) {
		Patient.formats = formats;
	}

	public LocalDate getBirthdate() {
		return birthDate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthDate = birthdate;
	}

	public int getHeight() {
		return (int) Height;
	}

	public void setHeight(double height) {
		Height = height;
	}

	public double getWeight() {
		return Weight;
	}

	public void setWeight(double weight) {
		Weight = weight;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void seeDoctor(Patient x) {
		System.out.print(x.getName() + " see the docotr");

	}

	public void seeDoctor() {
		System.out.println("go see the doctor.");

	}

}
