package phimsan.weeraphat.lab5;

import phimsan.weeraphat.lab4.*;

/* <h1>AccidentPatient
 *     keep the accidental patient information
 * @param isInICU typeOfAccident
 * 
 * @author Weeraphat Phimsan
 * 
 * @version 1.0
 * @since 2017-02-08
 */

public class AccidentPatient extends PatientV2 {

	private boolean isInICU; // show the status of AccidentPatient in ICU room
	private String typeOfAccident; // type of accident of accidental patient
	
//constructor
	public AccidentPatient(String name, String birthdate, Gender gender, double weight, double height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.isInICU = isInICU;
		this.typeOfAccident = typeOfAccident;
	}

//isInICU method : show the status of Accidental patient that is him or her in ICU?
	public boolean isInICU() {

		return isInICU;
	}

//getters and setters
	public void setisInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	protected void patientReport() {
		System.out.println("You are in an accident.");
	}

	@Override
	public String toString() {
		return "AccidentPatient " + "[" + getTypeOfAccident() + ", " + (isInICU() ? " In ICU" : " Not in ICU") +  "] " + "Patient [" + getName()
				+ ", " + getBirthdate() + ", " + getGender() + ", " + getWeight() + "kg., " + getHeight() + "cm.";

	}


}
