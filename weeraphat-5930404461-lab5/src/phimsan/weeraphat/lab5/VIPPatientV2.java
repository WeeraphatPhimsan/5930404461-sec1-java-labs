package phimsan.weeraphat.lab5;
/*
 * <p>Program:  VIPPatient
 * 
 * This program is similar to VIPPatient but has been added two more method  
 * feedPet playWithPet pay and pay(double amount)
 * 
 * @param passPhrase,totalDonation
 * @since 2017-02-09
 * @version 1.0
 * @author Weeraphat Phimsan
 * 
 * 
 * 
 */

import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab5.PatientV3.HasInsurance;
import phimsan.weeraphat.lab5.PatientV3.HasPet;

public class VIPPatientV2 extends PatientV3 implements HasInsurance, HasPet {

	
	private String passPhrase;
	private double totalDonation;

	public VIPPatientV2(String name, String birthdate, Gender gender, double weight, double height, double totalDonation,
			String passPhrase) {
		super(name, birthdate, gender, weight, height);
		this.passPhrase = passPhrase;
		this.totalDonation = totalDonation;
		setName(encrypt(name));
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	protected void patientReport() {
		System.out.println("Your record is private.");
	}

	@Override
	public String toString() {
		return "VIPPatient [ " + getName() + "," + getTotalDonation() + ", Patient [" + " " + getBirthdate() + ","
				+ getWeight() + "kg., " + getHeight() + "cm." + "]";
	}

	public String encrypt(String name) {
		int i = 0;
		String new_name = "";
		for (i = 0; i < name.length(); i++) {

			if (((name.charAt(i) + 7)) > 'z') {
				new_name = (new_name + ((char) ((name.charAt(i) + 7) - 26)));

			}
			if (((name.charAt(i) + 7)) <= 'z' && ((name.charAt(i) + 7)) >= 'a')
				new_name = new_name + ((char) ((name.charAt(i) + 7)));

		}

		return new_name;

	}

	public String decrypt(String name) {
		int i = 0;
		String new_name = getName();
		for (i = 0; i < name.length(); i++) {
			if ((name.charAt(i) - 7) < 'a') {
				new_name = (new_name + ((char) ((name.charAt(i) - 7) + 26)));

			}
			if ((name.charAt(i) - 7) <= 'z' && (name.charAt(i) - 7) >= 'a')
				new_name = new_name + ((char) ((name.charAt(i) - 7)));

		}
		new_name = new_name.substring(getName().length(), new_name.length());
		return new_name;
	}

	public String getVIPName(String passPhase) {

		if (passPhase == getPassPhrase()) {
			String VIPName = decrypt(getName());
			return VIPName;
		} else {
			System.out.print("Wrong passphrase please try again");
		}
		return "";

	}

	public double donate(double donate) {

		double donateMoney = donate;
		totalDonation = totalDonation + donateMoney;
		return totalDonation;

	}



	
	@Override
	public void feedPet() {
		System.out.println("feed the pet.");

	}

	@Override
	public void playWithPet() {
		System.out.println("pay with the pet");

	}

	@Override
	public void pay() {
		// TODO Auto-generated method stub

	}

	@Override
	public double pay(double amount) {
		
		return 0;
	}

}