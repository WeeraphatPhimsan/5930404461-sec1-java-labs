package phimsan.weeraphat.lab5;

import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab4.Patient;

public class PatientV2 extends Patient{


// constructor for PatientV2
public PatientV2(String name, String birthdate, Gender gender, double weight, double height) {
		super(name, birthdate, gender, weight, height);
		
	}

protected void patientReport(){
 System.out.println("You need medical attention.");	
}
}