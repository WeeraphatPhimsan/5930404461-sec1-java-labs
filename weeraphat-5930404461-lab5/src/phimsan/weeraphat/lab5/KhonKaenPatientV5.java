package phimsan.weeraphat.lab5;
import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab5.AccidentPatientV2;
import phimsan.weeraphat.lab5.TerminalPatientV2;
import phimsan.weeraphat.lab5.VIPPatientV2;

public class KhonKaenPatientV5 {
	public static void main(String[] arg) {
		AccidentPatientV2 piti = new AccidentPatientV2("piti", "12.01.2000", Gender.MALE, 65.5,
				169, "Car accident", true);
		TerminalPatientV2 weera = new TerminalPatientV2("weera", "15.02.2000", Gender.MALE, 72, 172, "Cancer",
				"01.01.2017");
		VIPPatientV2 duangjai = new VIPPatientV2("daungjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000,
				"mickeymouse");
		System.out.print(piti.getName() + " ");piti.seeDoctor();
		System.out.print(piti.getName());((AccidentPatientV2) piti).pay();
		piti.askPermission();
		System.out.print(weera.getName() + " ");weera.seeDoctor();
		System.out.print(weera.getName() + " ");weera.pay(2000);
		System.out.print(weera.getName() + " ");weera.askPermission("Somchai");

		System.out.print(duangjai.getVIPName("mickeymouse") + " ");
		duangjai.seeDoctor();
		System.out.print(duangjai.getVIPName("mickeymouse") + " ");duangjai.playWithPet();
		System.out.print(duangjai.getVIPName("mickeymouse") + " ");duangjai.feedPet();

	}
}
