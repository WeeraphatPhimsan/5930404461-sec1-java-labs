package phimsan.weeraphat.lab5;
/*
 * <h1>Class SickPeople<h1>
 * abstract class that has one abstract method call "seeDoctor"
 *@version 1.0
 *@author Weeraphat Phimsan
 *@since 2017-02-09
 */
public abstract class SickPeople {
 abstract void seeDoctor();
}
