package phimsan.weeraphat.lab5;
/*
 * This program is to compare the old of patients
 * @author weeraphat phimsan 593040446-1
 * @version 1.0
 * @since 2017-02-09
 */

import java.time.LocalDate;

import phimsan.weeraphat.lab4.*;

public class OlderPatient {
	public static void main(String[] str) {
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "Car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "12.02.2000", Gender.MALE, 72, 172, "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 68, 170, "Fire Accident", true);
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);

	}

	// isOldest method is to find the oldest people
	private static void isOldest(PatientV2 patient1, PatientV2 patient2, PatientV2 patient3) {

		String patient1Name = patient1.getName();
		String patient2Name = patient2.getName();
		String patient3Name = patient3.getName();
		// check whether the patient is VIP patient
		if (patient1 instanceof VIPPatient) {
			patient1Name = ((VIPPatient) patient1).getVIPName(((VIPPatient) patient1).getPassPhrase());
		}

		if (patient2 instanceof VIPPatient) {
			patient2Name = ((VIPPatient) patient2).getVIPName(((VIPPatient) patient2).getPassPhrase());
		}
		if (patient3 instanceof VIPPatient) {
			patient3Name = ((VIPPatient) patient3).getVIPName(((VIPPatient) patient3).getPassPhrase());
		}

		LocalDate old1 = (patient1.getBirthdate()); // keep birthday of the first patient 
		LocalDate old2 = (patient2.getBirthdate()); // keep birthday of the second patient
		LocalDate old3 = (patient3.getBirthdate());// keep birthday of the third patient

		int result1 = old1.compareTo(old2); // compare the birthday between first and second patient
		int result2 = old2.compareTo(old3);// compare the birthday between second and third patient
		int result3 = old1.compareTo(old3);// compare the birthday between first and third patient

		// case 1 : first patient is oldest
		if (result1 < 0 && result3 < 0) {
			System.out.println(patient1.getName() + " is oldest among " + patient1Name + " " + patient2Name + " and "
					+ patient3Name);
		}

		// case 2 : second patient is oldest
		if (result2 < 0 && result1 > 0) {
			System.out.println(patient2.getName() + " is oldest among " + patient1Name + " " + patient2Name + " and "
					+ patient3Name);
		}

		// case 3 : third patient is oldest
		if (result3 > 0 && result2 > 0) {
			System.out.println(patient3.getName() + " is oldest among " + patient1Name + " " + patient2Name + " and "
					+ patient3Name);
		}

	}

	/*
	 * isOlder method is to compare two patient and find the on is older
	 */
	private static void isOlder(PatientV2 patient1, PatientV2 patient2) {
		LocalDate old1 = (patient1.getBirthdate());
		LocalDate old2 = (patient2.getBirthdate());
		old1.compareTo(old2);

		// check who is older than another one
		// case 1: first patient is younger than second patient
		if (old1.compareTo(old2) < 0) {

			if (patient1 instanceof VIPPatient) {

				System.out.println(((VIPPatient) patient1).getVIPName(((VIPPatient) patient1).getPassPhrase())
						+ " is older than " + patient2.getName());
			}
			if (patient2 instanceof VIPPatient) {
				System.out.println(patient1.getName() + " is older than "
						+ ((VIPPatient) patient2).getVIPName(((VIPPatient) patient2).getPassPhrase()));
			} else {
				System.out.println(patient1.getName() + " is older than " + patient2.getName());
			}

		}
		// case 1: first patient is older than second patient
		if (old1.compareTo(old2) > 0) {
			if (patient1 instanceof VIPPatient) {

				System.out.println(((VIPPatient) patient1).getVIPName(((VIPPatient) patient1).getPassPhrase())
						+ " is not older than " + patient2.getName());
			}
			if (patient2 instanceof VIPPatient) {
				System.out.println(patient1.getName() + " is not older than "
						+ ((VIPPatient) patient2).getVIPName(((VIPPatient) patient2).getPassPhrase()));
			} else {
				System.out.println(patient1.getName() + " is not older than " + patient2.getName());
			}
		}

	}
}