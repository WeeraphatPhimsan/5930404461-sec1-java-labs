package phimsan.weeraphat.lab5;

/* <h1>AccidentPatient
 *     keep the accidental patient information
 * @param isInICU typeOfAccident
 * 
 * @author Weeraphat Phimsan
 * 
 * @version 2.0
 * @since 2017-02-08
 */
import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab4.Patient;
import phimsan.weeraphat.lab5.PatientV3.HasInsurance;
import phimsan.weeraphat.lab5.PatientV3.UnderLegalAge;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {

	private boolean isInICU;
	private String typeOfAccident;

	public AccidentPatientV2(String name, String birthdate, Gender gender, double weight, double height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.isInICU = isInICU;
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public void pay() {
		System.out.println(" pay the accident bill with insurance");
	}

	@Override
	public double pay(double amount) {
		System.out.println(" pay" + amount + "for his hospital visit by insurance.");
		return 0;

	}

	@Override
	public String askPermission(String legalGuardianName) {
		System.out.println(legalGuardianName + "ask parents for permission to cure him in an accident. ");
		return null;
	}

	@Override
	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident. ");

	}

}
