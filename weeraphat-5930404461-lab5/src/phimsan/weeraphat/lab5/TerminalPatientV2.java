package phimsan.weeraphat.lab5;
/* <h1>TerminalPatient
 *     this program is to keep the terminal patient information
 * @param firstDiagnosed the first date that found the disease 
 * @param terminalDisease the disease that has been found
 * 
 * @author Weeraphat Phimsan
 * 
 * @version 2.0
 * @since 2017-02-09
 */
import phimsan.weeraphat.lab4.Gender;
import phimsan.weeraphat.lab5.PatientV3.HasInsurance;
import phimsan.weeraphat.lab5.PatientV3.UnderLegalAge;

public class TerminalPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {

	public TerminalPatientV2(String name, String birthdate, Gender gender, double weight, double height, String string,
			String string2) {
		super(name, birthdate, gender, weight, height);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void pay() {
		// TODO Auto-generated method stub

	}

	@Override
	public double pay(double amount) {
		System.out.println("pay " + amount + "  for his hospital visit by insurance.");
		return 0;
	}

	@Override
	public void askPermission() {
	

	}

	@Override
	public String askPermission(String legalGuardianName) {
		
		System.out.println( "ask " + legalGuardianName + "for permission to treat with Chemo.");
		return null;
	}
}
