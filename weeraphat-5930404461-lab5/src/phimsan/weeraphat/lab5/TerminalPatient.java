package phimsan.weeraphat.lab5;
/* <h1>TerminalPatient
 *     this program is to keep the terminal patient information
 * @param firstDiagnosed the first date that found the disease 
 * @param terminalDisease the disease that has been found
 * 
 * @author Weeraphat Phimsan
 * 
 * @version 1.0
 * @since 2017-02-09
 */
import java.time.LocalDate;

import phimsan.weeraphat.lab4.Gender;

public class TerminalPatient extends PatientV2 {

	public TerminalPatient(String name, String birthdate, Gender gender, double weight, double height,
			String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, formats);
		this.terminalDisease = terminalDisease;
	}

	private LocalDate firstDiagnosed;
	private String terminalDisease;

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", " + firstDiagnosed + "], " + "Patient [" + getName() + ", "
				+ getBirthdate() + ", " + getGender() + ", " + getWeight() + "kg., " + getHeight() + "cm. " + "]";
	}

	protected void patientReport() {
		System.out.println("You have terminal illness");
	}

}
