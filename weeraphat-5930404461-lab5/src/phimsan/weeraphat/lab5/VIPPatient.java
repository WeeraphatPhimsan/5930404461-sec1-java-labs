package phimsan.weeraphat.lab5;

/*
 * <p>Program:  PatientsV2
 * 
 * This program is to keep the profile of VIP patient 
 * 
 * @param passPhrase,totalDonation
 * @since 2017-02-09
 * @version 1.0
 * @author Weeraphat Phimsan
 * 
 * 
 * 
 */

import phimsan.weeraphat.lab4.Gender;

public class VIPPatient extends PatientV2 {

	private String passPhrase; // this variable contain value of "String
								// passPhrase(the phrase that use for password)"
	private double totalDonation; // this variable contain value of(double)
									// totalDonation (numbers of donated money )

	
	// constructor
	public VIPPatient(String name, String birthdate, Gender gender, double weight, double height, double totalDonation,
			String passPhrase) {
		super(name, birthdate, gender, weight, height);
		this.passPhrase = passPhrase;
		this.totalDonation = totalDonation;
		setName(encrypt(name));
	}

	// getters and setters
	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	@Override
	protected void patientReport() {
		System.out.println("Your record is private.");
	}

	@Override
	public String toString() {
		return "VIPPatient [ " + getName() + "," + getTotalDonation() + ", Patient [" + " " + getBirthdate() + ","
				+ getWeight() + "kg., " + getHeight() + "cm." + "]";
	}

	// encrypt method: encrypting the name of patient with Caesar cipher
	public String encrypt(String name) {
		int i = 0;
		String new_name = "";
		for (i = 0; i < name.length(); i++) {

			if (((name.charAt(i) + 7)) > 'z') {
				new_name = (new_name + ((char) ((name.charAt(i) + 7) - 26)));

			}
			if (((name.charAt(i) + 7)) <= 'z' && ((name.charAt(i) + 7)) >= 'a')
				new_name = new_name + ((char) ((name.charAt(i) + 7)));

		}

		return new_name;

	}

	// decrypt method : is use to decrypt the encrypted VIP patient's name
	public String decrypt(String name) {
		int i = 0;
		String new_name = getName();
		for (i = 0; i < name.length(); i++) {
			if ((name.charAt(i) - 7) < 'a') {
				new_name = (new_name + ((char) ((name.charAt(i) - 7) + 26)));

			}
			if ((name.charAt(i) - 7) <= 'z' && (name.charAt(i) - 7) >= 'a')
				new_name = new_name + ((char) ((name.charAt(i) - 7)));

		}
		new_name = new_name.substring(getName().length(), new_name.length());
		return new_name;
	}

	// this method is to get password from use in order to unlock the encrypted
	// name
	public String getVIPName(String passPhase) {

		if (passPhase == getPassPhrase()) {
			String VIPName = decrypt(getName());
			return VIPName;
		} else {
			System.out.print("Wrong passphrase please try again");
		}
		return "";

	}

	// donate method : get the numbers of donations from user
	public double donate(double donate) {

		double donateMoney = donate;
		totalDonation = totalDonation + donateMoney;
		return totalDonation;

	}

}
