package phimsan.weeraphat.lab4;

/*
 * <h1>Program : KhonKaenPatientsV3<h1>
 * this program is to illustrate how to use method that in different package
 *
 *@param <String>name, <String>birthday, <Enum>gender, <double>weight, <double>height, <String>visit date, <String>admit date,  <String>discharge date
 *
 *@author Weeraphat Phimsan 593040446-1
 *
 *@Version 1.0
 *
 *
 *@since 31-01-2017
 *
 */


public class KhonKaenPatientsV3 {

	public static void main(String[] args) {

		Patient manee  = new InPatient ("Manee" , "01.12.1980", Gender.FEMALE, 60,   150, "20.01.2017", "29.01.2017");
		Patient mana   = new OutPatient("Mana"  , "22.04.1981", Gender.MALE,   70,   160, "23.01.2017");
		Patient chujai = new Patient   ("Chujai", "03.03.1980", Gender.FEMALE, 41.5, 175);
		System.out.println(manee);
		System.out.println(mana);
		System.out.println(chujai);

		if (isTaller(manee, mana)) {
			System.out.println(manee.getName() + " is taller thans" + mana.getName());
		} else {

			System.out.println(mana.getName() + " is taller than " + manee.getName());
		}

	}

	// isTaller method is to check the taller patient
	public static boolean isTaller(Patient personOne, Patient personTwo) {

		if (personOne.getHeight() > personTwo.getHeight()) {
			return true;
		} else {
			return false;
		}
	}

}
