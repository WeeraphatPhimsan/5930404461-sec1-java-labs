package phimsan.weeraphat.lab2;

import java.util.Arrays;

public class IntegersSorter {
public static void main ( String[] args ) {
	//check arguments____________________________________________________________________
	if( args.length == 1 ) {
	 System.err.println( "<the number of intergers to sort>_<i1> <i2>...<in>" );	 
	}

	// Before sorting 
	if( args.length > 1 )  {  
		System.out.print( "Before sorting: [ " );
	    for( int count = 0; count < args.length; count++ ) {
	             System.out.print( args[ count ] + " " );
	} System.out.println( "]" ); 


	//set the size of Arrnumber ( "Arrnumber" stands for Array of sorting number ) and enter numbers to array 
	final int Size = Integer.parseInt( args[ 0 ] );
	int[] Arrnumber =  new int[ Size ]; 	 
	    for( int count = 1; count < args.length; count++ ) {
	             Arrnumber[ count - 1 ] = Integer.parseInt( args[ count ] );
	}
						  
	    
	//sorting
	Arrays.sort( Arrnumber );


	// after sorting : print sorted numbers
	System.out.print( "\nAfter sorting: [ " );	

		 
    for( int count = 1; count < args.length ; count++ ) {
    	    System.out.print( Arrnumber[ count ] + " " );
	
    } System.out.println( " ]" ); 

    
   }	
	

  }
 

}