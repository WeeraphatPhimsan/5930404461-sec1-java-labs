package phimsan.weeraphat.lab2;

public class PatientV2 {
public static void main( String[] args ) {
	if( args.length != 4 )
	{    
System.err.println( "Patient <name> <gender> <weight> <height>" );
System.exit( 0 );
	}
	
	String gender = args[ 1 ];
	float weight = Float.parseFloat( args[ 2 ] );
	int height = Integer.parseInt( args[ 3 ] );
	

	if( ( weight < 0 ) || ( ( weight < 0 ) && ( height < 0 ) ) ) 
	{
	System.out.println( "This patient name is " + args[ 0 ] );
	System.out.println( "weight must be non-negative" );	
	}
	
	if( ( height < 0 ) && ( weight > 0 ) ) 
	{
	System.out.println( "This patient name is " + args[ 0 ] );
	System.out.println( "height must be non-negative" );	
	}
	
	
	if(  weight > 0  &&  height > 0  ){
		
	if ( gender.equalsIgnoreCase("Female") )
	{
	System.out.println( "This patient name is " + args[ 0 ] );
	System.out.println( "Her weight is " + weight +" kg." + "and height is "+ height + " cm.");
	}
	
		
	else if ( gender.equalsIgnoreCase("Male") )
	{
	System.out.println( "This patient name is " + args[ 0 ] );
	System.out.println( "His weight is " + weight +" kg." + "and height is "+ height + " cm.");
	}
	
	else
	{
		System.out.println( "This patient name is " + args[ 0 ] );
		System.out.println( "Please enter gender only Male or Female");
	}
       
	
	     }	
    }

}
