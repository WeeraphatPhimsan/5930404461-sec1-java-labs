/*
 * Program: TetrisGameV2
 * This codes has been download from teacher link in google class room
 * 
 * This program is to run the main tetris program by create object of TetrisPanelV2
 * and add it to the frame.
 * * 
 *  
 * @author Dr. Pattarawit Polpinit
 * @editor Weeraphat Phimsan 593040446-1
 * @version 2
 * @create 21.04.2016
 * 
 * */
package phimsan.weeraphat.lab11;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TetrisGameV2 extends JFrame {
	private static final long serialVersionUID = 1L;
	TetrisPanelV2 panel;

	public TetrisGameV2(String name) {
		super(name);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		TetrisGameV2 tetrisV2 = new TetrisGameV2("CoE Tetris Game V2");
		tetrisV2.addComponents();
		tetrisV2.setFrameFeatures();
	}

	protected void addComponents() {
		panel = new TetrisPanelV2();
		add(panel);
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setResizable(false);
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
	}
}
