/*
 * Program: TetrisGame
 * This codes has been download from teacher link in google class room
 * 
 * This program is to run the main tetris program by create object of TetrisPanel
 * and add it to the frame.
 * 
 * 
 *  
 * @author Dr. Pattarawit Polpinit
 * @editor Weeraphat Phimsan 593040446-1
 * @version 1
 * @create 18.04.2016
 * 
 * */
package phimsan.weeraphat.lab11;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TetrisGame extends JFrame {

	private static final long serialVersionUID = 1L;
	TetrisPanel panel;

	public TetrisGame(String name) {
		super(name);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		TetrisGame tetris = new TetrisGame("CoE Tetris Game");
		tetris.addComponents();
		tetris.setFrameFeatures();
	}

	protected void addComponents() {
		panel = new TetrisPanel();
		add(panel);
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (((dim.height - h) / 2)-200);
		pack();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		pack();
		setLocation(x, y);

	}
}
