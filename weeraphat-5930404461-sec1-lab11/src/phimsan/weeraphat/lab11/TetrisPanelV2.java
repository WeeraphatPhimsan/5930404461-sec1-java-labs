/*
 * Program: TetrisPanelV2
 * This codes has been download from teacher link in google class room
 * 
 * This program is to draw falling Tetromino piece that will fall from
 * the top of the scene to the bottom.
 * Tetromino piece that hit other piece that has landed on the bottom
 *  will stop and can't move the left or right if it collide other piece or bottom 
 *  of the scene.
 *  
 * 
 * 
 *  
 * @author Dr. Pattarawit Polpinit
 * @editor Weeraphat Phimsan 593040446-1
 * @version 2
 * @create 21.04.2016
 * 
 * */
package phimsan.weeraphat.lab11;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

public class TetrisPanelV2 extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 200, HEIGHT = 400;
	public static final int BOARD_WIDTH = 10, BOARD_HEIGHT = 20;
	public static final int SLEEP_TIME = 500;
	public static final int SQUARE_WIDTH = WIDTH / BOARD_WIDTH;
	public static final int SQUARE_HEIGHT = HEIGHT / BOARD_HEIGHT;

	protected int MOVE_LEFT = KeyEvent.VK_LEFT;
	protected int MOVE_UP = KeyEvent.VK_UP;
	protected int MOVE_RIGHT = KeyEvent.VK_RIGHT;
	protected int FALL_DOWN = KeyEvent.VK_DOWN;
	protected int ROTATE = KeyEvent.VK_UP;

	protected final int MAX_RECT = 600;

	ArrayList<StaticRectangle> notMovingRect = new ArrayList<StaticRectangle>();

	private Thread running;
	protected TetrisPieceV2 curPiece;

	public TetrisPanelV2() {
		super();
		setBackground(Color.LIGHT_GRAY);
		setSize(new Dimension(WIDTH, HEIGHT));

		curPiece = new TetrisPieceV2();
		setFocusable(true);
		addKeyListener(this);

		running = new Thread(this);
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		drawNotMovingRects(g2d);
		curPiece.draw(g2d);
	}

	/**
	 * Runs the game.
	 */
	public void run() {
		while (true) {
			if (curPiece.canMoveDown(notMovingRect)) {
				curPiece.moveDown();
			} else {
				// dump squares to be drawn later
				pieceNotMoving();

				// Generate new piece
				curPiece.reset();
			}
			repaint();

			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException ignore) {
				// Do nothing
			}

		}
	}

	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	protected void pieceNotMoving() {
		for (int i = 0; i < 4; ++i) {
			int x = curPiece.getPosX() + curPiece.getX(i);
			int y = curPiece.getPosY() + curPiece.getY(i);
			notMovingRect.add(new StaticRectangle(
					new Rectangle2D.Double(x * SQUARE_WIDTH, y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT),
					curPiece.getColor()));

		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == MOVE_RIGHT) {
			if (curPiece.canMoveRight(notMovingRect)) {
				curPiece.moveRight();
			}
		}
		if (e.getKeyCode() == MOVE_LEFT) {
			if (curPiece.canMoveLeft(notMovingRect)) {
				curPiece.moveLeft();
			}
		}
		if (e.getKeyCode() == MOVE_UP) {
			if (curPiece.canRotate()) {
				curPiece.rotate();
			}
		}
		if (e.getKeyCode() == FALL_DOWN) {
			if (curPiece.canMoveDown(notMovingRect)) {
				curPiece.fallDown(notMovingRect);
			} else {
				pieceNotMoving();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// Do nothing
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// Do nothing
	}

	protected void drawNotMovingRects(Graphics2D g2d) {
		for (int i = 0; i < notMovingRect.size(); i++) {
			g2d.setColor(notMovingRect.get(i).getColor());
			g2d.fill(notMovingRect.get(i).getRect());
			g2d.setColor(Color.DARK_GRAY);
			g2d.draw(notMovingRect.get(i).getRect());
		}
		repaint();
	}
}
