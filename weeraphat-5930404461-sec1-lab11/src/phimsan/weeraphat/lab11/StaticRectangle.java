/* 
 *
 * Program: StaticRectangle
 * This codes has been download from teacher link in google class room
 * 
 * This program is to be class for keep properties of each rectangle 
 * that be one part of all tetromino.
 * The constructor get to parameter that is Double data for properties of rectangle double
 * and color of each of rectangles.
 * 
 * 
 *  
 * @author Weeraphat Phimsan 593040446-1
 * @version 1
 * @create 21.04.2016
 * 
 * */

package phimsan.weeraphat.lab11;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.awt.geom.RectangularShape;

public class StaticRectangle {
	Double properties;// keep double values for rectangle properties
	Color color; // color for each rectangle

	public StaticRectangle(Double inProperties, Color color) {
		this.properties = inProperties;
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public Double getRect() {
		return properties;
	}

}