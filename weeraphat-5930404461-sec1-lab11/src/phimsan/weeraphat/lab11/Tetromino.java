/*
 * this is an ENUM for keep name of Tetromino shape
 * 
 * */
package phimsan.weeraphat.lab11;

public enum Tetromino {
	NO_SHAPE, S_SHAPE, Z_SHAPE, I_SHAPE, T_SHAPE, O_SHAPE, L_SHAPE, J_SHAPE;
}
