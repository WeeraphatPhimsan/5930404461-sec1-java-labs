package phimsan.weeraphat.lab9;

import java.awt.MenuItem;
import java.awt.RenderingHints.Key;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class PatientFormV8 extends PatientFormV7 {

	private static final long serialVersionUID = 1L;

	public PatientFormV8(String title) {
		super(title);
		
	}

	public static void createAndShowGUI() {
		PatientFormV8 patientV8 = new PatientFormV8("Patient Form V8");
		patientV8.addComponents();
		patientV8.addListener();
		patientV8.setFrameFeatures();
        patientV8.setShortCutKeys();	
	}
    public void setShortCutKeys(){
    	setMnemonicKey();
    	setAccelatorKey();
    	
    }
	public void setAccelatorKey() {
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		customMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		font_16.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
		font_20.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		font_24.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));

	}

	public void setMnemonicKey() {
		fileMenu.setMnemonic(KeyEvent.VK_F);
		openMI.setMnemonic(KeyEvent.VK_O);
		saveMI.setMnemonic(KeyEvent.VK_S);
		exitMI.setMnemonic(KeyEvent.VK_X);
		redMI.setMnemonic(KeyEvent.VK_R);
		blueMI.setMnemonic(KeyEvent.VK_B);
		greenMI.setMnemonic(KeyEvent.VK_G);
		colorMI.setMnemonic(KeyEvent.VK_L);
		configMenu.setMnemonic(KeyEvent.VK_C);
		customMI.setMnemonic(KeyEvent.VK_U);
		sizeMI.setMnemonic(KeyEvent.VK_Z);
		font_16.setMnemonic(KeyEvent.VK_6);
		font_20.setMnemonic(KeyEvent.VK_0);
		font_24.setMnemonic(KeyEvent.VK_4);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}
}
