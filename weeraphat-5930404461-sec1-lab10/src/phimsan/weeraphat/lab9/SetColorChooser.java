package phimsan.weeraphat.lab9;

import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SetColorChooser extends JColorChooser implements ChangeListener {
	protected JColorChooser colorChooser = new JColorChooser();
	protected JLabel title;

	public SetColorChooser() {

		// Set up color chooser for setting text color
		colorChooser.getSelectionModel().addChangeListener(this);
		colorChooser.setBorder(BorderFactory.createTitledBorder("Choose Text Color"));

	}


	@Override
	public void stateChanged(ChangeEvent arg0) {
		Color newColor = colorChooser.getColor();
	}
}
