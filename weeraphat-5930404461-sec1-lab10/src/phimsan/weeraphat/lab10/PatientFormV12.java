/*
 * Program : PatientForm 
 *This program is improved version of 'PatientV9' that has more features. 
 * The features of this program are :
 * - add patient information to the archive by click 'Add' button
 * - display the added patient by click 'Display' menu item
 * This version also has new sub menu named 'Data'
 * and its menu items : 'Display', 'Search', 'Sort', and 'Remove' 
 * in this version user can remove and search patient by use remove function
 * to remove patient or
 * search function to search a patient from Data menu 
 *  
 *
 * The propose of this program is to learn about 'Dynamic Array' and 
 * how to use it to keep added patient information to the program. 
 * 
 * 
 * @version 12
 * @date: 04.04.60
 * @author: Weeraphat Phimsan ID: 593040446-1
 */
package phimsan.weeraphat.lab10;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import phimsan.weeraphat.lab4.Patient;

public class PatientFormV12 extends PatientFormV11 {
	private static final long serialVersionUID = 1L;
	JTextField searchTxtField = new JTextField();// this text field is for enter
													// patient name
	JOptionPane searchDialog = new JOptionPane(); // this is dialog for display
													// search box for patient
													// name

	public PatientFormV12(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV12 patientV12 = new PatientFormV12("Patient Form V12");
		patientV12.addComponents();
		patientV12.setFrameFeatures();
		patientV12.addListener();
	}

	public void addListener() {
		super.addListener();
		searchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		// search action
		if (src == searchMI) {
			String searchName = JOptionPane.showInputDialog("Please enter the patient name");
			if (searchPatient(searchName) != null) {
				JOptionPane.showMessageDialog(null, searchPatient(searchName).toString());
			}
		}
		// if user want to remove patient from list
		if (src == removeMI) {
			String removeName = JOptionPane.showInputDialog("Please enter the patient name to remove ");
			removePatient(removeName);
		}
	}

	private Patient searchPatient(String searchName) {
		Patient toSearchPatient = null;
		int cancelCheck = 2;// variable to check if cancel command has been call
		for (Patient patient : patientStorage) {
			if (patient.getName().equalsIgnoreCase(searchName)) {
				toSearchPatient = patient;
			}
		}
		// to check if user don't enter any patient's name and click cancel
		if (toSearchPatient == null && cancelCheck != JOptionPane.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(null, searchName + " is not found");

		}
		return toSearchPatient;

	}

	// removePatient is method to remove patient that user want to remove from
	// list
	private void removePatient(String removeName) {
		int cancelCheck = 2;// variable to check if cancel command has been call
		Patient toRemovePatient = null;
		for (Patient patient : patientStorage) {
			if ((patient = searchPatient(removeName)) != null) {
				patientStorage.remove(patient);
				displayPatients();
			} else if (toRemovePatient == null && cancelCheck != JOptionPane.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, removeName + " is not found");

			}
		}

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
