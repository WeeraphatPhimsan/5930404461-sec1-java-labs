/*
 * Program: 'TetrisPanel' Java program   
 * This program has been create in order to repeat and practice 
 * using of GUI for creating picture  
 * 
 * In this program,there has a only interface of game Tetris   
 * , the interface has shown seven types of Tetrimino shape and 
 * game name.
 * 
 * @param XPos[ position on 'X' axis] , coordinate of each shapes 
 * @create: 27.03.2017
 * @version: 1.0
 * @Author: Weeraphat Phimsan 593040446-1 Laboratory Section : 01
 * 
 * */
package phimsan.weeraphat.lab7;

import java.awt.*;

import javax.swing.*;

public class TetrisPanel extends JPanel {
	int tetrimino_Shape;
	Color color; // keep color of shape that want to draw	
	TetrisShape shapeColor = new TetrisShape(); // object of class 'TetrisShape  for use method "getColor" from class TetrisShape
	Font tetrisFont = new Font("Serif", Font.BOLD, 30); // set font's properties to draw 'Tetris' string
	int redVar, greenVar, blueVar, xPos, tetrimino_Order;// from left to right,
	// redVar greenVar and blueVar is stand for red,green, and blue value for keep number of red green and blue color 
	// xPos is variable that keep initial position on x axis
	// and tetrimino_Order is variable that keep order of each tetriminos.
	protected final int panelWidth = 600, panelHeight = 400, verticalBlocks = 30, horizontalBlocks = 20; //set the size of panel 
	//verticalBlock and horizonyalBlock is the value of blocks in vertical line and horizontal line.
	final int SIZE = 20, Y_POS = ((panelHeight / 2) - 40);
	int[][] axisPos = TetrisShape.getCoord();


	// constructor of this class
	public TetrisPanel() {
		this.setBackground(Color.LIGHT_GRAY);
		this.drawTetrimino(Tetrimino.S_SHAPE, 10);
	}
	
	///////////////////////////////// this is only extra line
	///////////////////////////////// /////////////////////////////////////////////////



	@Override
	// below massage show the value of each shapes
	// S_SHAPE(1), Z_SHAPE(2), I_SHAPE(3), T_SHAPE(4), O_SHAPE(5), J_SHAPE(6),
	// L_SHAPE(7);
	public void paint(Graphics graphic) {
		super.paintComponent(graphic);
		graphic.setFont(tetrisFont); // set font to draw
		graphic = (Graphics2D) graphic;
		graphic.setColor(Color.RED);
		graphic.drawString("Tertis", panelWidth / 2 - 40, panelHeight / 4);

		// loop for drawing all of seven tetrimino shape
		for (int i = 0; i < 7; i++) {

			switch (i) {
			case 0: // case 0 is draw S_SHAPE
				xPos = axisPos[0][1];
				xPos = 20;
				graphic.setColor(shapeColor.getColor(Tetrimino.Z_SHAPE));
				graphic.fillRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
			case 1:// case 1 is draw Z_SHAPE
				xPos = axisPos[1][1];
				xPos += 90;
				graphic.setColor(shapeColor.getColor(Tetrimino.S_SHAPE));
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS + (2 * SIZE), SIZE, SIZE);
			case 2:// case 2 is draw I_SHAPE
				xPos = axisPos[2][1];
				xPos += 170;
				graphic.setColor(shapeColor.getColor(Tetrimino.I_SHAPE));
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (3 * SIZE), SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (3 * SIZE), SIZE, SIZE);
			case 3:// case 3 is draw T_SHAPE
				xPos = axisPos[3][1];
				xPos += 230;
				graphic.setColor(shapeColor.getColor(Tetrimino.T_SHAPE));
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos + (2 * SIZE), Y_POS, SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos + (2 * SIZE), Y_POS, SIZE, SIZE);
			case 4:// case 3 is draw O_SHAPE
				xPos = axisPos[4][1];
				xPos += 330;
				graphic.setColor(Color.YELLOW);
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + SIZE, SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS + SIZE, SIZE, SIZE);
			case 5:// case 5 is draw L_SHAPE
				xPos = axisPos[5][1];
				xPos += 440;

				graphic.setColor(Color.ORANGE);
				graphic.fillRect(xPos - SIZE, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (SIZE), SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos - SIZE, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (SIZE), SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
			case 6: // case 6 is draw J_SHAPE
				xPos = axisPos[6][1];
				xPos += 510;
				graphic.setColor(Color.BLUE);
				graphic.fillRect(xPos, Y_POS, SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (SIZE), SIZE, SIZE);
				graphic.fillRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.fillRect(xPos + SIZE, Y_POS, SIZE, SIZE);
				graphic.setColor(Color.DARK_GRAY);
				graphic.drawRect(xPos, Y_POS, SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (SIZE), SIZE, SIZE);
				graphic.drawRect(xPos, Y_POS + (2 * SIZE), SIZE, SIZE);
				graphic.drawRect(xPos + SIZE, Y_POS, SIZE, SIZE);

			}
		}
	}

	protected void drawTetrimino(Tetrimino shape, int initial_X) {
		if (shape.equals("S_SHAPE")) {
			xPos = initial_X;
			tetrimino_Order = shape.ordinal();
			repaint();
		} else if (shape.equals("Z_SHAPE")) {
			xPos = initial_X;
			tetrimino_Order = shape.ordinal();
			repaint();
		}
	}

	// method setShape is use for setting the shape by ordinal of shape's name
	// in Enum Tetrimino
	public int setShape(Tetrimino shape) {
		if (shape.ordinal() == 0) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else if (shape.ordinal() == 1) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else if (shape.ordinal() == 2) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else if (shape.ordinal() == 3) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else if (shape.ordinal() == 4) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else if (shape.ordinal() == 5) {
			tetrimino_Order = shape.ordinal();
			return tetrimino_Order;
		} else {
			tetrimino_Order = Tetrimino.L_SHAPE.ordinal();
			return tetrimino_Order;
		}
	}

	// set the panel size
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(panelWidth, panelHeight);

	}

}
