/*
 * 
 * class TetrisPanelV3
 * This is program for creat something looks like in problem 2
 * This program not only set position on 'x' axis randomly but also 
 * randomly set the color of rectangle that fall from
 * the top of the screen each rounds.
 * 
 * If the rectangle reach the bottom of the screen, it will stay there not disappear from the screen.
 * And the next moving rectangle will fall from the top of the screen agian.
 * 
 * 
 * 
 * @create: 27.03.2017
 * @version: 1.0
 * @Author: Weeraphat Phimsan 593040446-1 Laboratory Section : 01
 * 
 * 
 * */
package phimsan.weeraphat.lab7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.util.ArrayList;

public class TetrisPanelV3 extends TetrisPanelV2 {

	private static final long serialVersionUID = 1L;
	int  numNotMovingRect = 0, redRandom, greenRandom, blueRandom;
	// above variables : numNotMovingRect is number of not moving rectangle , 
	// redRandom , greenRandom ,and blueRandom is keep value of [random]red green and blue color 
	final int SPEED = 8, RECT_WIDTH = 60, RECT_HEIGHT = 40;// set the speed and size of rectangle 
	Color color; // this variable keep the value of color of current falling rectangle
    Thread runningObj; // object of class thread 
	Rectangle2D.Double movingRect;// object of Rectangle2D.Double class 
	// for draw falling rectangle
	ArrayList<Rectangle2D.Double> notMovingRect = new ArrayList<Rectangle2D.Double>();
	int[] notMovingRectPos = new int[50];
	Color[] notMovingRectColor = new Color[50];

	public TetrisPanelV3() {
		this.setBackground(Color.white);
		this.repaint();
		runningObj = new Thread(this);
		runningObj.start();
	}

	// this method is for random the color of the rectangle that will fall from the top of panel
	// by randomly set the value of red green and blue, then return the value of new color
	public void colorRand() {
		redRandom = (int) (Math.random() * (255 - 0)) + 1;
		greenRandom = (int) (Math.random() * (255 - 0)) + 1;
		blueRandom = (int) (Math.random() * (255 - 0)) + 1;
		color = new Color(redRandom, greenRandom, blueRandom);
	}
//paint method
	public void paint(Graphics graphic) {
		super.paintComponent(graphic);
		Graphics2D graphic2d = (Graphics2D) graphic;
		movingRect = new Rectangle2D.Double(xPosit, yPosit, RECT_WIDTH, RECT_HEIGHT);

		
		//This is condition to check if the number of not moving rectangle greater than zero.
		// This loop is draw and fill all not moving rectangle and next moving rectangle 
		if (numNotMovingRect > 0) {
			for (int index = 0; index < notMovingRect.size(); index++) {
				graphic2d.setColor(notMovingRectColor[index]);
				graphic2d.fill(notMovingRect.get(index));
				graphic2d.setColor(Color.BLACK);
				graphic2d.draw(notMovingRect.get(index));
				graphic2d.setColor(notMovingRectColor[index]);
				repaint();	
			}
			graphic2d.setColor(color);
			graphic2d.fill(movingRect);
			graphic2d.setColor(Color.BLACK);
			graphic2d.draw(movingRect);
			repaint();

		}
// This condition is to check the number of not moving object, if it  is zero 
// It will draw the first rectangle of this program
		if (numNotMovingRect == 0) {
			graphic2d.setColor(color);
			graphic2d.fill(movingRect);
			graphic2d.setColor(Color.BLACK);
			graphic2d.draw(movingRect);
			repaint();
		}

	
	}

	@Override
	public void run() {

		while (true) {

			if (numNotMovingRect == 0 && yPosit == 0) {
				colorRand();
			}
			try {
				Thread.sleep(SPEED);

			} catch (InterruptedException ex) {
			}

			// check if the rectangle has reached the bottom of screen
			// the set the old color and old x position for draw the not moving objct
			if ((yPosit + SPEED) + RECT_HEIGHT >= HEIGHT) {
				notMovingRectColor[numNotMovingRect] = color;
				notMovingRectPos[numNotMovingRect] = xPosit;
			
			}
			
			// increase the position of y axis
			yPosit++;
			
			
			// this is the condition that linked with above condition 
			// this is to check if it reach the bottom of screen.
			// If it is true, it will randomly set new position of 'x' axis, set position of 'y' axis to zero.
			// add old moving rectangle to Array list of not moving rectangle to draw the not moving rectangle
			// and randomly set new color for next moving rectangle and if numNotMoving rectangle reach maximum point of 
			// array list size, it will reset the number of to the first member again.
			// This will not make the array index out of its bound. 
			if (yPosit == (panelHeight - 70) + 1) {

				xPosit = (int) (Math.random() * (600 - 0)) + 0;
				yPosit = 0;

				if (numNotMovingRect < 50) {
					numNotMovingRect++;
				}
				if (numNotMovingRect == 50) {
					numNotMovingRect = 0;
				}
				colorRand();
				notMovingRect.add(movingRect);
			}
		}
	}

}
