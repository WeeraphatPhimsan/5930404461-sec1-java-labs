package phimsan.weeraphat.lab7;
/*
 * Class Tetris
 *this is the classfor run program in lab7 problem 1
 * 
 * @create: 27.03.2017
 * @version: 1.0
 * @Author: Weeraphat Phimsan 593040446-1 Laboratory Section : 01
 * 
 * 
 * */
import java.awt.*;

import javax.swing.*;
import javax.swing.SwingUtilities;

public class Tetris extends JFrame {

	TetrisPanel gamePanel = new TetrisPanel();//object of class TetrisPanel

	public Tetris(String title) {
		super(title);
	}

	static void createAndShowGUI() {
		Tetris tetris = new Tetris("COE Tetris Game");// set title of window
		tetris.addComponents();
		tetris.setFrameFeatures();
	}

	protected void addComponents() {
		this.add(gamePanel); // add object of TetrisPanel
	}

	protected void setFrameFeatures() {
		this.setLocation(((Toolkit.getDefaultToolkit().getScreenSize().width / 2) - 300),
				(Toolkit.getDefaultToolkit().getScreenSize().height / 2) - 200);
		this.setSize(getPreferredSize());
		this.setVisible(true);
		this.setResizable(false);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}

		});

	}

}
