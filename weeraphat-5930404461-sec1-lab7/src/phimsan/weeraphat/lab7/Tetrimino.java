package phimsan.weeraphat.lab7;

// this is ENUM for keep name of all tetrimonos 
public enum Tetrimino {
	S_SHAPE, Z_SHAPE, I_SHAPE, T_SHAPE, O_SHAPE, J_SHAPE, L_SHAPE;

}