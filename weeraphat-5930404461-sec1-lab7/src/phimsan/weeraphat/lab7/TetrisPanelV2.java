/*
 * Program : TetrisPanel 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */ 
package phimsan.weeraphat.lab7;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.Timer;

public class TetrisPanelV2 extends TetrisPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	Timer clock; // Timer object 
	Thread running;	// Thread variable
	int counter=0,yPosit = 0, xPosit = panelWidth/2;
	// from left to right : counter is variable to  count how many times does the block fall?
	
	final int SPEED = 15; // set speed of falling block

	public TetrisPanelV2() {
		this.setBackground(Color.black);
		this.repaint();
		running = new Thread(this);
		running.start();
	}

	@Override
	public void paint(Graphics graphic) {
		super.paintComponent(graphic);
		graphic.setColor(Color.YELLOW);
		graphic.fillRect(xPosit, yPosit, 50, 50);
		repaint();

	}

	@Override
	public void run() {
		while (true) {
			//this is condition to check that if the block falls for the first time
			// if the condition is true ,it will appear on the middle of panel. 
			yPosit++; // y position increase
			if(counter == 0 ){
				xPosit = panelWidth/2;
				
				yPosit =0;
				counter++;
			// counter is 0 : is the first round then increase the value of it 	
				
			}
			
			
	// this is the condition to check if the block moving pass the bottom of the screen .
			// Then the program will set 'new x position' randomly
			if (yPosit > 400) {
				xPosit =(int)(Math.random() * (600-0)) + 0;
				yPosit = 0;
			}
			try {
				Thread.sleep(SPEED);
                             
			} catch (InterruptedException ex) {

			}
		}
	
	}
}
