
/* class TetrisShape 
 * 
 * This class has been create in order to keep value of colors and coordinates
 * of all Tetrismino shapes.
 * 
* @param XPos[ position on 'X' axis] , coordinate of each shapes 
 * @create: 27.03.2017
 * @version: 1.0
 * @Author: Weeraphat Phimsan 593040446-1 Laboratory Section : 01
 */
package phimsan.weeraphat.lab7;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class TetrisShape {
	Tetrimino shapeTetris;
	Color shapeColor;

	
	// coordinate of each shapes
	static int[][] coordinate = new int[][] { { 0, -1 }, { 0, 0 }, { -1, 0 }, { -1, 1 }, // coordinate of S_SHAPE
			{ 0, -1 }, { 0, 0 }, { 1, 0 }, { 1, 1 }, // coordinate of  coordinate of  Z_SHAPE
			{ 0, -1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, // coordinate of  I_SHAPE
			{ -1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 1 }, //coordinate of  T_SHAPE
			{ 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 }, //coordinate of  O_SHAPE
			{ -1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 }, // coordinate of  L_SHAPE
			{ 1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } // coordinate of  J_SHAPE

	};
	
	
	//color of each shapes
	Color I_SHAPE_COLOR = new Color(0, 255, 255);
	Color S_SHAPE_COLOR = new Color(0, 255, 0);
	Color Z_SHAPE_COLOR = new Color(255, 0, 0);
	Color T_SHAPE_COLOR = new Color(170, 0, 255);
	Color O_SHAPE_COLOR = new Color(255, 255, 0);
	Color L_SHAPE_COLOR = new Color(255, 165, 0);
	Color J_SHAPE_COLOR = new Color(0, 0, 255);

	//below method is to return the color of each shapes
	// by put argument as name of the shape you want
	protected Color getColor(Tetrimino shape) {
		if (shape == Tetrimino.Z_SHAPE) {
			return Z_SHAPE_COLOR;
		} else if (shape == Tetrimino.S_SHAPE) {
			return S_SHAPE_COLOR;
		} else if (shape == Tetrimino.I_SHAPE) {
			return I_SHAPE_COLOR;
		} else if (shape == Tetrimino.L_SHAPE) {
			return L_SHAPE_COLOR;
		} else if (shape == Tetrimino.T_SHAPE) {
			return T_SHAPE_COLOR;
		} else if (shape == Tetrimino.J_SHAPE) {
			return J_SHAPE_COLOR;
		} else {
			return O_SHAPE_COLOR;
		}
	}
// this is method to retur coordinate
	public static int[][] getCoord() {
		return coordinate;
	}


}
