/*
 * Program Tetris_V2 
 * 
 * Class Tetris
 *this is the classfor run program in lab7 problem 2
 * 
 * @create: 27.03.2017
 * @version: 1.0
 * @Author: Weeraphat Phimsan 593040446-1 Laboratory Section : 01
 * 
 * 
 * */

package phimsan.weeraphat.lab7;

import javax.swing.SwingUtilities;

public class TetrisV2 extends Tetris {

	TetrisPanelV2 tetrisPanelV2 = new TetrisPanelV2(); // create object of TetrisPanleV2 class
	// to add it to this window 

	public TetrisV2(String title) {
		super(title);

	}

	static void createAndShowGUI() {
		TetrisV2 tetrisV2 = new TetrisV2("Tetris V2");
		tetrisV2.addComponents();
		tetrisV2.setFrameFeatures();
	}

	@Override
	protected void addComponents() {
		this.add(tetrisPanelV2);
	}
// main method of this class
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();
			}

		});

	}

}
