package phimsan.weeraphat.lab7;

import java.awt.Component;

import javax.swing.SwingUtilities;

public class TetrisV3 extends TetrisV2 {
	TetrisPanelV3 tetrisPanelV3 = new TetrisPanelV3();

	public TetrisV3(String title) {
		super(title);

	}

	static void createAndShowGUI() {
		TetrisV3 tetrisV3 = new TetrisV3("Rectangles Dropping V2");
		tetrisV3.addComponents();
		tetrisV3.setFrameFeatures();
	}

	@Override
	protected void addComponents() {
		this.add(tetrisPanelV3);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});

	}
}
