/* Program : GuessNumberGame
 * this program has been created for 
 * practice the using of "control statements" 
 * Author : Weeraphat Phimsan 593040446-1 
   Created date : January 24,2017 */

package phimsan.weeraphat.lab3;

import java.util.Scanner;

public class GuessNumberGame {
	public static void main(String[] args) {

		// remGuesses is variable for keep the value of remaining guess

		int remGuesses = 7, answer;
		answer = 0 + (int) (Math.random() * ((100 - 0) + 1));
		Scanner guess = new Scanner(System.in);
		int number;

		do {

			// game start from here

			System.out.println("Number of remaining guess is " + remGuesses);
			System.out.print("Enter a guess: ");
			number = guess.nextInt();

			if (number == answer) {
				System.out.println("Correct !");
				break;
			}

			else if (number > answer) {
				System.out.println("Lower!");
				remGuesses--;
			}

			else if (number < answer) {
				System.out.println("Higher!");
				remGuesses--;
			}

			if (remGuesses == 0) {
				break;
			}

		} while (number != answer);


		if (remGuesses == 0) {
			System.out.println("You ran out of guesses. The number was " + answer);
		}
		guess.close();

	}

}
