/* Program : GuessNumberMethodGame 
 * this program has been created in the propose 
 * for practice the using of "control statements" 
 * creator : Weeraphat Phimsan 593040446-1 
   Created date : January 24,2017 */


package phimsan.weeraphat.lab3;

import java.util.Scanner;

public class GuessNumberMethodGame {

	static int remainingGuess = 7; // number of possible guess
	static int answer; // the number that player have to guess

	public static void main(String[] args) {
		genAnswer();
		playGame();
	}

	// genAnswer Method - this subroutine is for generating answer
	public static void genAnswer() {
		answer = 0 + (int) (Math.random() * ((100 - 0) + 1));
	}

	// playGame Method - this subroutine is for playing the game
	public static void playGame() {
		int number;
		Scanner choice = new Scanner(System.in);
		Scanner userGuess = new Scanner(System.in);

		do {
			System.out.println("Number of remaining guess is " + remainingGuess);
			System.out.print("Enter a guess : ");
			number = userGuess.nextInt();
			if (number == answer) {
				System.out.println("correct!");
			} else if (number > answer) {
				System.out.println("Lower!");
				remainingGuess--;
			} else if (number < answer) {
				System.out.println("Higher!");
				remainingGuess--;
			}
			if (remainingGuess < 1) {
				System.out.println("You ran out of guesses. The number was " + answer);
				break;
			}

		} while (number != answer);

		choice.close();
		userGuess.close();
	}
}
