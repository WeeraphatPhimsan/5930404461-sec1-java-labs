/*
 * Program : GuessNumberGameV2 
 * this program has been created  
 * for practice the using of "control statements" 
 * Author : Weeraphat Phimsan 593040446-1 
   Created date : January 24,2017 */

package phimsan.weeraphat.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {

	static int answer;

	public static void main(String[] args) {

		Scanner range = new Scanner(System.in);
		Scanner guess = new Scanner(System.in);
		Scanner choice = new Scanner(System.in);

		int number = 0, posGuess;
		char select;

		do {

			// game start from here
			System.out.print("Enter min and max of random numbers: ");
			int minRange = range.nextInt();
			int maxRange = range.nextInt();

			if (minRange > maxRange) {
				int changer;
				changer = minRange;
				minRange = maxRange;
				maxRange = changer;
			}

			answer = minRange + (int) (Math.random() * ((maxRange - minRange) + 1));
			System.out.print("Enter the number possible guess : ");
			posGuess = guess.nextInt();

			do {
				System.out.println("Number of remaining guess is " + posGuess);
				System.out.print("Enter a guess ");
				number = guess.nextInt();

				if (number <= minRange) {
					System.out.println("Numbers must be in " + minRange + " and " + maxRange);
				}

				if (number >= maxRange) {
					System.out.println("Numbers must be in " + minRange + " and " + maxRange);
				}

				if (number > minRange && number < maxRange) {
					if (number == answer) {
						System.out.println("Correct!");
						System.out.print("Want to play agian? ( \"Y\" or \"y\" ) : ");
						select = choice.next().charAt(0);
						break;
					}

					if (number < answer) {
						System.out.println("Higher!");
						posGuess--;

					}

					if (number > answer) {
						System.out.println("Lower!");
						posGuess--;
					}

					if (posGuess < 1) {
						System.out.println("You ran out of guesses. The number was " + answer);
						System.out.print("Want to play agian? ( \"Y\" or \"y\" ) : ");
						select = choice.next().charAt(0);
						break;
					}

				}

			} while (number != answer);

		} while (number != answer);
		
		range.close();
		guess.close();
		choice.close();
	}
}
