/**
 * This program PatientFormV2 extends from PatientFormV1 
 * The program should have the following properties:
 * 1. The title of the program should be �Patient Form V2�
 * 2. Add the label �Gender�� and two radio buttons �Male and �Female�
 * 3. Add the label �Address:� and the text area with 2 rows and 35 columns.   
 * 4. Create the scroll pane and add the text area in that scroll pane.  
 * 5. Initialize the text area with the content as "Department of Computer Engineering, 
 * Faculty of Engineering, Khon Kaen University, Mittraparp Rd., T. Naimuang, 
 * A. Muang, Khon Kaen, Thailand, 40002"
 * 6. Place the Gender and the Address parts above the two buttons. 
 * 
 * @author: Kanda Saikaew
 * @date:  21/02/2017
 * @version: 1.0
 */
package phimsan.weeraphat.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class PatientFormV2 extends PatientFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3694909354550437224L;

	protected JRadioButton maleRadioB, femaleRadioB;
	protected ButtonGroup genderGrpB;
	protected JTextArea addrTxtArea;
	protected JScrollPane addrScrollPane;
	protected JPanel genderPanel, addrPanel, contentPanel, genderChoicesPanel;
	protected JLabel genderLabel, addrLabel;

	public final static int NUM_TXTAREA_ROWS = 2;
	public final static int NUM_TXTAREA_COLS = 35;

	public PatientFormV2(String title) {
		super(title);
	}

	protected void initComponents() {
		super.initComponents();
		maleRadioB = new JRadioButton("Male");
		femaleRadioB = new JRadioButton("Female");
		genderGrpB = new ButtonGroup();
		genderPanel = new JPanel(new GridLayout(1, 2));
		genderChoicesPanel = new JPanel();
		addrTxtArea = new JTextArea(NUM_TXTAREA_ROWS, NUM_TXTAREA_COLS);
		addrTxtArea.setLineWrap(true);
		addrTxtArea.setWrapStyleWord(true);
		addrTxtArea.setText("Department of Computer Engineering, Faculty of Engineering, ");
		addrTxtArea.append("Khon Kaen University, Mittraparp Rd., T. Naimuang, ");
		addrTxtArea.append("A. Muang, KhonKaen, Thailand, 40002");
		addrScrollPane = new JScrollPane(addrTxtArea);
		addrPanel = new JPanel(new BorderLayout());
		contentPanel = new JPanel(new BorderLayout());
		genderLabel = new JLabel("Gender:");
		addrLabel = new JLabel("Address:");
	}

	protected void addComponents() {
		super.addComponents();
		genderChoicesPanel.add(maleRadioB);
		genderChoicesPanel.add(femaleRadioB);
		genderPanel.add(genderLabel);
		genderPanel.add(genderChoicesPanel);
		genderGrpB.add(maleRadioB);
		genderGrpB.add(femaleRadioB);
		addrPanel.add(addrLabel, BorderLayout.NORTH);
		addrPanel.add(addrScrollPane, BorderLayout.SOUTH);
		contentPanel.add(textsPanel, BorderLayout.NORTH);
		contentPanel.add(genderPanel, BorderLayout.CENTER);
		contentPanel.add(addrPanel, BorderLayout.SOUTH);
		overallPanel.add(contentPanel, BorderLayout.NORTH);
	}

	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
