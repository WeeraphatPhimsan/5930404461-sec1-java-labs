/*
 * 
 * Program: PatientForm 
 * This program has been created in order to learn about how to set mnemonic and accelerator key.
 * 
 * This program is extended from PatientFormV8 in package 'phimsan.weeraphat.lab9',
 * in this program has improved threes more feature that are user can change color customly
 * by using color chooser from custom color menu item,
 * when user click save or open menu item save or open dialog will apear.
 * 
 * If user click save or open button the dialog will show message "You have (saved/opened) file 'file name' ".
 * If user click cancel the dialog will show message "(Save/Open) command is cancelled".
 * @author Weeraphat Phimsan ID: 5930404446-1 Section: 1
 * @create  27/03/2016
 * @version 9
 * 
 * */
package phimsan.weeraphat.lab9;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PatientFormV9 extends PatientFormV8 {

	private static final long serialVersionUID = 1L;
	//declare and create object for file chooser and color chooser
	JFileChooser fileChooser = new JFileChooser();
	JColorChooser colorChooser = new JColorChooser();

	//constructor of the class PatientFormV9
	public PatientFormV9(String title) {
		super(title);
	}

	//Method create and show GUI is to set default properties of the frame.
	public static void createAndShowGUI() {
		PatientFormV9 patientV9 = new PatientFormV9("Patient Form V9");
		patientV9.addComponents();
		patientV9.addListener();
		patientV9.setFrameFeatures();
		patientV9.setShortCutKeys();
	}
	
	
	
	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		// check if user click save sub menu 
		if (event.getSource() == saveMI) {

			int returnVal = fileChooser.showSaveDialog(PatientFormV9.this);
			File selectedFile = fileChooser.getSelectedFile();
			//check if user click OK button in save dialog
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(null, "You have saved file " + selectedFile.getName());
			} else {
				JOptionPane.showMessageDialog(null, "Save command is cancelled");
			}
		}
		// check if user click open sub menu 
		if (event.getSource() == openMI) {
			int retValue = fileChooser.showOpenDialog(PatientFormV9.this);
			File selectedFile = fileChooser.getSelectedFile();
			//check if user click OK button in open dialog or not
			if (retValue == fileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(null, "You have opened file " + selectedFile.getName());
			} else {
				JOptionPane.showMessageDialog(null, "Open command is cancelled ");
			}
		}
		// check if user click custom or call custom(color) menu item 
		if (event.getSource() == customMI) {
			Color color = Color.BLACK;
			Color colorChooserPane = colorChooser.showDialog(null, "Select a color", addrTxtArea.getForeground());
			if (color == Color.BLACK || color != Color.BLACK) {
				changeFontColor(colorChooserPane);
			}

		}
	}//end of  method actionPerformed
//////////////////////////////////////////////////////////////////////
	
	// add listener to open save and custom(color) menu item
	public void addListener() {
		super.addListener();
		exitMI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		customMI.addActionListener(this);
	}

	//below this line is the main method to run this program
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}//end of class
}
