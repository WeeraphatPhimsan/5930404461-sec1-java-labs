/*
 * 
 * Program: PatientForm 
 * This program has been created in order to learn about how to set mnemonic and accelerator key.
 * 
 * This program is extended from PatientFormV7 in package 'phimsan.weeraphat.lab9',
 * in this program has improved two more features that is each of menu items has its 
 * mnemonic key and accelerator key.
 * 
 * 
 * @author Weeraphat Phimsan ID: 5930404446-1 Section: 1
 * @create  27/03/2016
 * @version 8
 * 
 * */

package phimsan.weeraphat.lab9;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class PatientFormV8 extends PatientFormV7 {

	private static final long serialVersionUID = 1L;
	//create new menu item 'Custom' for customize the font size
	JMenuItem customFont = new JMenuItem("Custom...");

	//constructor of the class
	public PatientFormV8(String title) {
		super(title);

	}
    // method create and show GUI
	public static void createAndShowGUI() {
		PatientFormV8 patientV8 = new PatientFormV8("Patient Form V8");
		patientV8.addComponents();
		patientV8.addListener();
		patientV8.setFrameFeatures();
		patientV8.setShortCutKeys();// calling method set short cut key
	}

	public void addComponents() {
		super.addComponents();
		sizeMenu.add(customFont);// add custom menu to size menu item
	}

	// this method is to set shortcut key of menu items
	public void setShortCutKeys() {
		setMnemonicKey();
		setAcceleratorKey();
	}

	// This method is to set accelerator key of menu items
	public void setAcceleratorKey() {
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
	    greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		customMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		font_16.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		font_20.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		font_24.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		customFont.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,
				   Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
	
	}// end of method setAcceleratorKey

	// This method is to set mnemonic key of menu item
	public void setMnemonicKey() {
		// set mnemonic key of menu file and its sub menu
		fileMenu.setMnemonic(KeyEvent.VK_F);
		openMI.setMnemonic(KeyEvent.VK_O);
		saveMI.setMnemonic(KeyEvent.VK_S);
		exitMI.setMnemonic(KeyEvent.VK_X);
		redMI.setMnemonic(KeyEvent.VK_R);
		blueMI.setMnemonic(KeyEvent.VK_B);
		greenMI.setMnemonic(KeyEvent.VK_G);
		colorMenu.setMnemonic(KeyEvent.VK_L);
		// set mnemonic key of menu "Config" and its sub menu
		configMenu.setMnemonic(KeyEvent.VK_C);
		customMI.setMnemonic(KeyEvent.VK_U);
		sizeMenu.setMnemonic(KeyEvent.VK_Z);
		font_16.setMnemonic(KeyEvent.VK_6);
		font_20.setMnemonic(KeyEvent.VK_0);
		font_24.setMnemonic(KeyEvent.VK_4);
		customFont.setMnemonic(KeyEvent.VK_M);
	}// end of method setMnemonicKey
	public void actionPerformed(ActionEvent event){
		super.actionPerformed(event);
	}
    public void addLIstener(){
    	super.addListener();
    	
    }
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}//end of the class
}
