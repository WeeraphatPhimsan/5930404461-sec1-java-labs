/*
 * Program: PatientForm 
 * This program has been created in order to learn about using of "JSlider" and "Change listener".
 * 
 * This program is extended from PatientFormV6 in package 'phimsan.weeraphat.lab8',
 * in this program has improved one more feature that is 'blood pressure' of patient 
 * by using "JSlider" of 'top' and 'bottom' number you can adjust the blood pressure
 * of each patients.When user changed the top number or bottom number of blood pressure,
 * the update dialog will appear with message that tell the user that top or bottom 
 * number has been change to new value.
 * And when the user click OK button the information dialog will show message 
 * of patient's information with two new information that is top and bottom number
 * of blood pressure.
 * 
 * 
 * @author Weeraphat Phimsan ID: 5930404446-1 Section: 1
 * @create  27/03/2016
 * @version 7
 * 
 * 
 * */
package phimsan.weeraphat.lab9;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import phimsan.weeraphat.lab8.PatientFormV6;

public class PatientFormV7 extends PatientFormV6 implements ChangeListener {

	// <<<<<<<<<<<<<<<<decalring object>>>>>>>>>>>>>>>
	JPanel bloodPresPanel, new_AddedInfoPanel;// blood pressure panel is to
												// contain the JSlider
	// blood pressure of top and bottom number. And new_AddedInforPanel for
	// adjusting the area for
	// radio button of gender and JSlider of blood pressure
	JSlider bloodSliderTop, bloodSliderButtom;// JSlider for top number and bottom number of blood pressure
	JLabel topNumLabel, buttomNumLabel;// this is label for top number and bottom number JSlider
	TitledBorder bloodPresTitle;// this is object for set area of blood pressure information

	
	
	
	//constructor of this class
	public PatientFormV7(String title) {
		super(title);
	}

	//method create and show GUI
	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.addListener();
		patientForm7.setFrameFeatures();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// his condition is to check if user click "OK" button
		// - show the dialog with information that typed by user
		if (event.getSource() != okButton) {
			super.actionPerformed(event);
		}
		if (event.getSource() == okButton) {
			// update dialog
			JOptionPane.showMessageDialog(null,
				 "Name = "            + nameTxtField.getText()      + 
				 " Birthdate = "      + dateTxtField.getText()      +   
				 " Weight = "	      + weightTxtField.getText()    + 
				 "Height = "          + heightTxtField.getText()    + 
				 "\n" + "Gender = "   + (maleRadioB.isSelected() ? "Male" : "Female")  + 
				 "\nAddress = "       + addrTxtArea.getText()	    + 
				 "\n" + "Type = "     + typeList.getSelectedItem()  + 
				 "\nBlood Pressure :" + 
				 bloodSliderTop.getName()    + " = " + bloodSliderTop.getValue() + 
				 ", " + bloodSliderButtom.getName() + " = " + bloodSliderButtom.getValue());

		}

	}

	public void addListener() {
		super.addListener();
    //set the name of JSlider and add ChangeListener to each JSliders
		bloodSliderTop.setName("Top number");
		bloodSliderTop.addChangeListener(this);
		bloodSliderButtom.setName("Buttom number");
		bloodSliderButtom.addChangeListener(this);
	}

	
	@Override
	public void addComponents() {
		super.addComponents();
	//below code is to set new layout of the program in order to add 
		// two JSlider to the panel under the gender radio button area
		
		
		//set layout of area for JSlider to title Border
		bloodPresTitle = BorderFactory.createTitledBorder("Blood Pressure");
		bloodPresPanel = new JPanel(new GridLayout(2, 2));
		///////////////////////////////////////////////////////////////
		//set the properties of JSlider and panel for JSlider
		topNumLabel    = new JLabel("Top number:");
		buttomNumLabel = new JLabel("Buttom number:");
		bloodSliderTop = new JSlider(0, 200);
		bloodSliderTop.setMajorTickSpacing(50);
		bloodSliderTop.setMinorTickSpacing(10);
		bloodSliderTop.setPaintTicks(true);
		bloodSliderTop.setPaintLabels(true);
        ///////////////////////////////////////////////////////////////
		bloodSliderButtom = new JSlider(0, 200);
		bloodSliderButtom.setMajorTickSpacing(50);
		bloodSliderButtom.setMinorTickSpacing(10);
		bloodSliderButtom.setPaintTicks(true);
		bloodSliderButtom.setPaintLabels(true);
		new_AddedInfoPanel = new JPanel(new BorderLayout());
		bloodPresPanel.setBorder(bloodPresTitle);
		bloodPresPanel.add(topNumLabel);
		bloodPresPanel.add(bloodSliderTop);
		bloodPresPanel.add(buttomNumLabel);
		bloodPresPanel.add(bloodSliderButtom);

		// add to new Panel created in this class
		//(new_AddedInfoPanel is the new panel is to add panel of radio button and panel of JSlider
		// to the main panel of the program)
		new_AddedInfoPanel.add(genderPanel, BorderLayout.NORTH);
		new_AddedInfoPanel.add(bloodPresPanel, BorderLayout.CENTER);

		// then add new_AddedInfoPanel to the main panel 
		contentPanel.add(new_AddedInfoPanel, BorderLayout.CENTER);

	}// end of method addComponents

	
	// this is the main method to run this program
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

	// stateChanged method is to check if user change value of top number or bottom number
	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			int value = source.getValue();
			String name = source.getName();
			JOptionPane.showMessageDialog(null, name + " of Blood Pressure is " + value);
		}

	}// end of the class

}
