/*
 * Program Patient Form V5
 * This program is version 5 of PatientForm.java
 * The program has more features added from PatientFormV4
 * of this program are 'setting size of font' and 'changing the color of font'.
 *  
 *  The two feature above you can access via "Configuration menu[Config]" menu on menu bar 
 *  and then choose 'Size' for set the size of font or choose color to set the color of font.
 *  
 *  
 *  this program has five extra methods that are
 *  menuInitializer -> is to set the name of menu or menu item
 *  setInitFont -> is to set initial font of text field or text area
 *  manageSubContents -> is to add or do anything with sub menu or menu item
 *  changeFontColor-> is to changed color of font
 *  changedFontSize -> is to changed size of font
 *  
 *  
 *  
 *  Sizes that available  are 16 , 20 ,and 24.
 *  Colors that available are red, green, and blue.
 *  
 * @author: Weeraphat Phimsan 593040446-1
 * @version 5
 * @date: 15/03/60
 * */

package phimsan.weeraphat.lab8;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.*;

import phimsan.weeraphat.lab6.*;

public class PatientFormV5 extends PatientFormV4 {

	private static final long serialVersionUID = 1L;
	protected JMenuItem blueMI, greenMI, redMI, customMI, font_16, font_20, font_24;
	//blurMI redMI green MI is menu item to be the choice of available font color
	// font_16 font_20 and font_24 is the size of font
	
	

	Font font; // declare font variable
	protected JMenu colorMenu; // these are menu for select color and font size
	protected JMenu sizeMenu;

	public PatientFormV5(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");

		patientForm5.addComponents();
		patientForm5.addListener();
		patientForm5.setFrameFeatures();

	}

	@Override
	protected void addListener() {
		super.addListener();// use addListener of super class
		// add listener to sub menus of color and size of font
		redMI.addActionListener(this);
		blueMI.addActionListener(this);
		greenMI.addActionListener(this);
		font_16.addActionListener(this);
		font_20.addActionListener(this);
		font_24.addActionListener(this);
	}

	// this method is to add sub menu to main menu on menu bar
	public void manageSubContents() {
		// add all sub menu to their proper categories

		// add available color list to 'Color' sub menu
		colorMenu.add(blueMI);
		colorMenu.add(greenMI);
		colorMenu.add(redMI);
		colorMenu.add(customMI);
		// add available font size to 'Size' sub menu
		sizeMenu.add(font_16);
		sizeMenu.add(font_20);
		sizeMenu.add(font_24);
	}

	// this method is to set the initial properties like font size of text field
	public void setInitFont() {
		font = new Font("Sanserif", Font.TRUETYPE_FONT, 13);
		// set font to all text field
		nameTxtField.setFont(font);
		dateTxtField.setFont(font);
		heightTxtField.setFont(font);
		weightTxtField.setFont(font);
		addrTxtArea.setFont(font);
	}

	// this method is to change size of font by pass 'number' of size you want
	// to change
	// Example: changed FontSize(24);
	// The font will change to size 24.
	public void changeFontSize(int fontSize) {
		font = new Font("Sanserif", Font.BOLD, fontSize);
		nameTxtField.setFont(font);
		dateTxtField.setFont(font);
		heightTxtField.setFont(font);
		weightTxtField.setFont(font);
		addrTxtArea.setFont(font);
	}

	// this method is to change color of font by pass 'color' you want to change
	// Example: changed FontSize(Color.YELLOW);
	// The color of font will change to yellow.
	public void changeFontColor(Color color) {
		nameTxtField.setForeground(color);
		dateTxtField.setForeground(color);
		heightTxtField.setForeground(color);
		weightTxtField.setForeground(color);
		addrTxtArea.setForeground(color);
	}

	// this method is to set the name of menu or sub menu
	public void menuInitializer() {

		colorMenu = new JMenu("Color"); // initialize title of menu 'color'
		sizeMenu = new JMenu("Size"); // initialize title of menu 'size'
		// initialize title of sub menu in menu 'color'
		redMI = new JMenuItem("Red");
		greenMI = new JMenuItem("Green");
		blueMI = new JMenuItem("Blue");
		customMI = new JMenuItem("Custom...");
		// initialize title of sub menu in menu 'size'
		font_16 = new JMenuItem("16");
		font_20 = new JMenuItem("20");
		font_24 = new JMenuItem("24");

	}

	@Override
	public void addComponents() {
		super.addComponents();// call addComponent method of super class
		menuInitializer();
		manageSubContents();
		setInitFont();
		// add color menu and size menu to Config menu

		colorMI.setVisible(false);
		sizeMI.setVisible(false);
		configMenu.add(colorMenu);
		configMenu.add(sizeMenu);

	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object selectedMenu = event.getSource();

		// These condition below is to check what font size or color you had
		// selected
		// change the size of font
		// by call the method 'changeFontSize'
		if (selectedMenu == font_16) {
			changeFontSize(16);

		} else if (selectedMenu == font_20) {
			changeFontSize(20);

		} else if (selectedMenu == font_24) {
			changeFontSize(24);
		}
		// change the color of font
		// by call the method 'changeFontColor'
		if (selectedMenu == redMI) {
			changeFontColor(Color.RED);
		} else if (selectedMenu == greenMI) {
			changeFontColor(Color.GREEN);
		} else if (selectedMenu == blueMI) {
			changeFontColor(Color.BLUE);
		}
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

}
