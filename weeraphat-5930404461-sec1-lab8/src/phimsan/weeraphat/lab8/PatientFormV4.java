/*
 * Program Patient Form V4
 * This program is version 4 of PatientForm.java
 * The program has more features added from PatientFormV3
 * that is when use click on 'OK' button the dialog
 * of input information will appear and  when use click on 'cancel' button 
 * all text will be erased.
 * 
 * @author: Weeraphat Phimsan 593040446-1
 * @version 4
 * @date: 15/03/60
 * */
package phimsan.weeraphat.lab8;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import phimsan.weeraphat.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {
	private static final long serialVersionUID = -8336532530097397466L;
	JDialog informDialog = new JDialog(); // inform dialog is to show message of
											// patient'sinformation
	String massage;// keep massage that

	public PatientFormV4(String string) {
		super(string);

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

	@Override
	protected void addComponents() {
		super.addComponents();
		super.addMenus();

	}

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.addListener();
		patientForm4.setFrameFeatures();

	}

	protected void addListener() {
		super.okButton.addActionListener(this);// add action listener to 'OK'
												// button
		super.cancelButton.addActionListener(this);// add action listener to
													// 'Cancel' button
		super.maleRadioB.addItemListener(this);// add item listener to
												// 'male' radio button
		super.femaleRadioB.addItemListener(this);// add item listener to
													// 'female' radio button
		super.typeList.addItemListener(this);// add item listener to 'list'
												// in combo box
	}

	// actionPerformed method
	@Override
	public void actionPerformed(ActionEvent event) {
	
		// this condition is to check if user click "OK" button
		// - show the dialog with information that typed by user
		if (event.getSource() == okButton) {
			JOptionPane.showMessageDialog(null,"Name = " + nameTxtField.getText() + " Birthdate = " + dateTxtField.getText() + " Weight = "
					+ weightTxtField.getText() + "Height = " + heightTxtField.getText() + "\n" + "Gender = "
					+ (maleRadioB.isSelected() ? "Male" : "Female") + "\nAddress = " + addrTxtArea.getText()
					+ "\n" + "Type = " + typeList.getSelectedItem());
		
	
		
		}

		// this condition check if user click "Cancel" button
		// - clear all text in all text fields and the text area
		if (event.getSource() == cancelButton) {

			nameTxtField.setText(" ");
			dateTxtField.setText(" ");
			weightTxtField.setText(" ");
			heightTxtField.setText(" ");
			addrTxtArea.setText("");
		}

	}

	@Override
	// method item state changed is to check the state of radio button or combo
	// box has changed
	public void itemStateChanged(ItemEvent itemEvent) {
		JLabel genderMessage = new JLabel();
		JLabel typeMessage = new JLabel();
		JPanel newButtonPanel = new JPanel(new BorderLayout());
		JPanel newButtonPanel2 = new JPanel(new BorderLayout());
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
	
		// object selectedRadButton for get source object that is radio button
		Object selectedRadButton = itemEvent.getSource();
		JDialog genderUpdate = new JDialog();
		JDialog typeUpdate = new JDialog();
		// this condition is check if user changed gender
		// - show dialog with message "Your gender is now changed to male or
		// female"
		if (itemEvent.getStateChange() == ItemEvent.SELECTED
				&& (selectedRadButton == maleRadioB || selectedRadButton == femaleRadioB)) {
			// "Your gender type is now changed to " + (selectedRadButton ==
			// maleRadioB ? "male" : "female"));
			// below command is to set default gender dialog
			genderUpdate.setLayout(new BorderLayout(0, 20));
			genderMessage = new JLabel(
					"  Your gender type is now changed to " + (selectedRadButton == maleRadioB ? "male" : "female"));
			genderUpdate.setVisible(true);
			genderUpdate.setSize(300, 150);
			genderUpdate.setLocation(x, (2 * y + 60));
			genderUpdate.setTitle("Gender info");
			genderUpdate.add(genderMessage, BorderLayout.NORTH);
			newButtonPanel.add(okButton, BorderLayout.EAST);
			genderUpdate.add(newButtonPanel, BorderLayout.CENTER);
			genderUpdate.pack();

		}

		// this condition check if user change the patient's type
		// - show the dialog with message "YOur patient type is now changed to
		// [male or female] "
		if (itemEvent.getSource() == typeList) {
			if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
				typeMessage = new JLabel("Your patient type is now changed to " + typeList.getSelectedItem());
				typeUpdate.setLayout(new BorderLayout());
				typeUpdate.setTitle("Message");
				typeUpdate.setSize(500, 150);
				typeUpdate.setLocation(x, (2 * y + 60));
				typeUpdate.setVisible(true);
				typeUpdate.add(typeMessage, BorderLayout.NORTH);
				newButtonPanel2.add(okButton,BorderLayout.EAST);
				typeUpdate.add(newButtonPanel2, BorderLayout.CENTER);
				typeUpdate.pack();
			}
		}

	}

}
